# Silhouette Engine Contribution Guidelines

[Back to README](../README.md)

Before starting a new feature, please create an issue to discuss it.

Contributors are expected to adhere to the [code of conduct](conduct.md).  It
really isn't that hard, just don't be a jerk, and everything will be fine.