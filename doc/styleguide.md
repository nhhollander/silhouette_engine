# Source Code Style Guideline

[Back to README](../README.md)

*Note: These are **guidelines** and not meant to be the final law of the
program.  If you believe your particular situation is most intuitively handled
in a way contrary to this guide, trust is placed in individual developers to
make that decision*

## Headers (.h/.hpp)

### Style

Header files should be written in 80 columns or less where possible.
Declarations that exceed 80 columns are only permitted where wrapping the
declaration would make the line harder to read, or result in a single parameter
being wrapped to a new line.

Example of when wrapping is appropriate:

```c++
void function_with_unrealistically_long_name_because_reasons(int var1,
    int var2, int var3, int var4, int var5, int var6);
```

Example of when wrapping is inappropriate:

```c++
void another_function_with_an_even_more_unrealistically_long_name_foo(
    int bar);
```

All declarations should be commented where appropriate.  Comments should explain
what a declaration is *for*, *how* it is intended to be used, and *why* it is
the way that it is.

Example of a well commented declaration:

```c++
/*!
 *  X position of this entity in 3d space.
 *
 *  During each frame cycle, the renderer will draw (if appropriate) this
 *  entity at this position in 3D space.  Writes to this variable require
 *  that the `foobar` mutex be locked.
 */
volatile float pos_x = 0.00;
```

Example of a poorly commented declaration:

```c++
/*!
 *  X position.
 */
volatile float pos_x = 0.00;
```

### File Organization

Firstly each header file should begin with a header that includes the name of
the file, a brief description of the purpose of the file, and a license
disclaimer.  See the following example:

```c++
/*!
 *  @file <path_to_file>/<name_of_file>.hpp
 *
 *  A brief description of the file should go here.  For classes this may be
 *  "This file defines the <xyz> class".
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */
```


Each header file should have an #include guard of the following format, with
the slashes in the path replaced with underscores.

```c++
#ifndef _PATH_TO_FILE_H_
#define _PATH_TO_FILE_H_

// ... code ...

#endif
```

## Sources (.c/.cpp)

### Style

Source files should be limited to 80 columns where doing so is convenient, and
does not impact the readability of the code.  Limiting the number of columns
in source files is less important than header files, but should still be
attempted.

Functions and methods should occupy as few lines as possible to complete a
single discrete task.  For example, a function `load_config_file()` that is
responsible for loading, and parsing configuration files should not be written
as:

```c++
void load_config_file() {
    // Read file
    // ... open and read file to memory ...

    // Parse file
    // ... parse file into components ...

    // Apply config
    // ... save configuration values to various locations in memory ...
}
```

Instead it should be broken into separate private methods:

```c++
const char* read_file() {
    // ... open and read file to memory ...
}
const char* parse_config_data() {
    // ...  parse file into components ...
}
void apply_config() {
    // ... save configuration values to various locations in memory...
}
void load_config_file() {
    // Read file
    read_file();
    // Parse file
    parse_config_data();
    // Apply config
    apply_config();
}
```

### File Organization

Each source file should begin with a header that contains the name of the file,
and a license disclaimer.  See the following example:

```c++
/*!
 *  @file <path_to_file>/<name_of_file>.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */
```

The first `#include` directive in each source file should refer to its header
file. The second block of `#include` directives should be for headers within the
project.  The third and final block should `#include` system libraries as
required.  See the following example:

```c++
#include "foo.hpp"

#include "bar.hpp"
#include "baz.hpp"

#include <stdio.h>
#include <string>
```

Methods defined in the class should be sorted under header blocks corresponding
with their accesability, with the exception of the constructor and destructor,
which should be defined at the top of the file, right after the `#include`
directives.  See the following example:

```c++
// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

// ... constructor and destructor ...

// ===================
// = PRIVATE METHODS =
// ===================

// ... private methods ...

// ==================
// = PUBLIC METHODS =
// ==================

// ... public methods ...
```

Example source file that defines class `Foo`:

```c++
/*!
 *  @file foo.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "foo.hpp"

#include "bar.hpp"
#include "baz.hpp"

#include <bap>
#include <bep>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

Foo::Foo() {
    // Construction of class Foo
}

Foo::~Foo() {
    // Destruction of class Foo
}

// ===================
// = PRIVATE METHODS =
// ===================

void Foo::bip() {
    // Private method bip
}

void Foo:bop() {
    // Private method bop
}

// ==================
// = PUBLIC METHODS =
// ==================

void Foo:bup() {
    // Public method bup
}
```