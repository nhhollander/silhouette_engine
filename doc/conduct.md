# Silhouette Engine Project Code of Conduct

[Back to README](../README.md)

1. Be excellent to each other

This code of conduct is taken from Brian Lunduke.  For more information, see
[his page](http://www.lunduke.com/coc.html).

Behavior that violates these terms should be reported to nhhollander@wpi.edu
including *"SE Code of Conduct"* in the subject line.  Specific detauls of
reports will be kept confidential, however a redacted version may be released
separately.