#include "util/config.hpp"

#include <cstdio>

using namespace util;

int main() {
    // Create the dynamic configuration container
    Configuration config("test_configuration");

    // Load a configuration file
    config.load("test.cfg");

    /* Load another configuration file.  The overwrite parameter is not set, so
     * duplicate values will be ignored. */
    config.load("test2.cfg");
    
    /* Get a value from the container.  If "test.value" was not found in either
     * of the configuration files we loaded, the default value of 3 will be
     * returned. */
    int value = config.get_int("test.value", 3);

    /* Set a value in the container.  If "test.value2" is not found, a warning
     * will be generated because the create parameter is not set. */
    config.set("test.value2", 1234);

    /* Set a value in the container, creating the container if required.  In
     * this situation, missing configuration values will be created and
     * populated instead of generating a warning. */
    config.set("test.value3", "potato_salid_is_good", true);

    /* Get a pointer to a value.  If you are planning on frequently accessing
     * a value, it's more efficient to do it through a pointer, as the process
     * for looking up and returning a value every time is relatively
     * computationally expensive.
     * If "test.value" is not found, nullptr will be returned.  It's always a
     * good idea to check the pointer before you try to use it, to avoid
     * segmentation faults. */
    const volatile int* value_pointer = config.get_intp("test.value");

    /* Get a pointer to a configuration value object.  If "test.value3" is not
     * found, nullptr will be returned. It's important to check for this to
     * prevent segmentation faults and other program crashes. */
    ConfigurationValue* cv = config.get("test.value3");

    /* Create a change handler and assign it to the configuration value.  This
     * function will be invoked every time the configuration value is updated
     * by calling one of the "set" functions. */
    cv->add_change_handler([](ConfigurationValue* value, Configuration* config){
        printf("Configuration [test.value3] has been changed to [%s]!",
            value->string_);
    });

    /* Change the value of "test.value3".  This change will automatically
     * propagate the value to the other variable types, and invoke the change
     * handler defined above. */
    cv->set("New Value!");
}