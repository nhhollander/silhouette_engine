# ItemDB item database

[Back to README](../README.md)

**Development of ItemDB is currently suspended while I work on other, more
important things, like programming an engine, so that I can program a game, so
that I have something that could actually use this engine.**

The ItemDB system is responsible for tracking and storing information about
virtual items.  It is capable of tracking detailed information and attributes
for every item that exists or existed in the past, as well as ensuring
consistancy between multiple game servers.

## Master Server

The master server holds the authoritative copy of the item database, as well as
the archive and storage databases.

### Active Item Table

Items in the active item table are still present in player inventories, banks,
and other storage mediums.  These items have not been crafted, sold, or
destroyed.  This table is generally stored in the memory of the master server.

| Column    | Size (B) | Description                                           |
|-----------|----------|-------------------------------------------------------|
| Item ID   | 8        | Globally unique identifier for this item              |
| Item Type | 3        | Item template identifier                              |
| Status    | 1        | See [Status](#status)                                 |
| User ID   | 4        | Account ID of the user who owns this item             |
| Source ID | 8        | ID of the item that was destroyed to create this      |
| CMethod   | 1        | What action caused this item to be created            |
| Created   | 4        | Timestamp of when this item was created               |
| Flags     | 2        | Queryable item attributes                             |
| Attribs   | Variable | Compressed item attributes                            |

### Archive Item Table

Items in the archive table that have been destroyed are moved to this table, and
information about the events that caused their destruction are added.  This
table is generally stored on a high-capacity SSD or HDD, as speedy access to its
contents is not required.

| Column    | Size (B) | Description                                           |
|-----------|----------|-------------------------------------------------------|
| *prev*    | *31+*    | *All columns from the Active Item Table*              |
| Fate ID   | 8        | ID of the item that was created from this one         |
| DMethod   | 1        | What action caused this item to be destroyed          |
| Destroyed | 4        | Timestamp of when this item was destroyed             |

### Cold Storage Item Table

Items in the cold storage tables have been destroyed along with every item in
their ancestry.  This table is generally stored in a compressed format on a HDD,
as it exists for archival purposes only.  This table uses the same format as the
archive item table.

## Features

### Status

Item status' are used to indicate the location or fate of an item, and can be
any one of the following values
| Status    | Value | Description                          |
|-----------|-------|--------------------------------------|
| INVENTORY | 0x00  | Item is in player's inventory        |
| BANK      | 0x01  | Item is in the bank                  |