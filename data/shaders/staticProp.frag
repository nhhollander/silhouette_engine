// Deferred render output buffers
layout(location = LOC_OUT_POS) out vec3 g_position;
layout(location = LOC_OUT_NORM) out vec3 g_normal;
layout(location = LOC_OUT_ALB_SPEC) out vec4 g_albedo_spec;

// Texture inputs
layout(location = LOC_TEX_ALBEDO) uniform sampler2D albedo_sampler;
layout(location = LOC_TEX_SPECULAR) uniform sampler2D specular_sampler;

// Misc. inputs
in vec2 uv;
in vec3 frag_pos;
in vec3 normal;

void main() {
    // Store the position
    g_position = frag_pos;
    // Store base normals
    g_normal = normal;
    // Calculate albedo
    g_albedo_spec.rgb = texture(albedo_sampler, uv).rgb;
    // Calculate specular (stored in alpha component)
    g_albedo_spec.a = texture(specular_sampler, uv).r;
}