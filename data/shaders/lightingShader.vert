layout(location = LOC_IN_VERT) in vec3 vertex;
layout(location = LOC_IN_UV) in vec2 uv_in;

out vec2 uv;

void main() {
    // Pass the vertices directly through
    gl_Position = vec4(vertex, 1);
    // Pass the UV directly through
    uv = uv_in;
}