// Input uv coordinates calculated in vertex shader
in vec2 uv;

// Color output to screen
layout(location = LOC_OUT_COLOR) out vec3 color;

// Texture samplers
layout(location = LOC_TEX_POS)      uniform sampler2D g_position;
layout(location = LOC_TEX_NORM)     uniform sampler2D g_normal;
layout(location = LOC_TEX_ALB_SPEC) uniform sampler2D g_albedo_spec;
layout(location = LOC_TEX_DEPTH)    uniform sampler2D g_depth;

// Inputs
layout(location = LOC_IN_DYN_LIGHT_COUNT) uniform int dynamic_light_count;
layout(location = LOC_IN_CAM_POS) uniform vec3 camera_position;

// Lighting input structure (do not change names)
struct Light {
    vec4 position;
    vec3 color;
};
// Lighting uniforms (Do not change variable name)
uniform Light dynamic_lights[MAX_DYNAMIC_LIGHTS];

void main() {

    // Get base data
    vec3 position = texture(g_position, uv).rgb;
    vec3 normal = texture(g_normal, uv).rgb;
    vec4 alb_spec = texture(g_albedo_spec, uv);
    vec3 albedo = alb_spec.rgb;
    float specular = alb_spec.a;
    
    // Calculate view direction
    vec3 view_dir = normalize(camera_position - position);

    // Calculate dynamic lights
    vec3 lighting = albedo * 0.1;
    for(int i = 0; i < dynamic_light_count; i++) {

        // Calculate vector between light and pixel location
        vec3 light_dir = normalize(dynamic_lights[i].position.xyz - position);
        float dir_factor = dot(light_dir, normal);

        #if SPECULAR_ENABLED == 1
        // Calculate specular factor
        vec3 spec_vec = normalize(view_dir + light_dir);
        float spec_factor = dot(spec_vec, normal);
        if(spec_factor <= 0) {
            spec_factor = 0.0;
        } else {
            spec_factor = pow(spec_factor, 80);
        }
        // Determine the result of the specular shader
        vec3 specular_res = dynamic_lights[i].color * spec_factor;
        #endif

        // Calculate deltas
        float dx = dynamic_lights[i].position.x - position.x;
        float dy = dynamic_lights[i].position.y - position.y;
        float dz = dynamic_lights[i].position.z - position.z;
        // Calculate the inverse square of the distance
        float dist_factor = 10 / (dx*dx + dy*dy + dz*dz);

        // Calculate the final result of the diffuse calculation
        vec3 diffuse_res = dist_factor * albedo * dynamic_lights[i].color * dir_factor;

        // Combine the result of this dynamic light and add to the accumulator
        #if SPECULAR_ENABLED == 1
        lighting += diffuse_res + specular_res;
        #else
        lighting += diffuse_res;
        #endif
    }
    
    // Done
    color = lighting;
}