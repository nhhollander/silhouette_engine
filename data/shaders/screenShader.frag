// Input uv coordinates calculated in vertex shader
in vec2 uv;

// Color output to screen
out vec3 color;

// Texture sampler
layout(location = LOC_TEX_SCRTEX) uniform sampler2D screen_texture;


void main() {
    // Calculate screen space coordinates
    vec2 screen_pos = vec2((uv.x * 2) - 1, (uv.y * 2) - 1);
    // Calculate distance from center
    float dist = sqrt((screen_pos.x * screen_pos.x) + (screen_pos.y * screen_pos.y));
    // Calculate a vignette
    float vign = 1 - (dist / 6);

    // Different render process depending on supersample mode
    #if SUPERSAMPLE_FACTOR > 1
        // Calculate pixels
        int x = 0;
        int y = 0;
        int pcount = SUPERSAMPLE_FACTOR * SUPERSAMPLE_FACTOR;
        ivec2 sf = ivec2(gl_FragCoord.xy);
        vec3 acc = vec3(0,0,0);
        for(y = 0; y < SUPERSAMPLE_FACTOR; y++) {
            for(x = 0; x < SUPERSAMPLE_FACTOR; x++) {
                ivec2 nf = ivec2((sf.x * SUPERSAMPLE_FACTOR) + x, (sf.y * SUPERSAMPLE_FACTOR) + y);
                acc += texelFetch(screen_texture, nf, 0).rgb;
            }
        }
        // Get the color
        color = acc / pcount;
    #else 
        // Simple texture passthrough
        color = texture(screen_texture, uv).xyz * vign;
    #endif
}