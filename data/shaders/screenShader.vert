layout(location = LOC_IN_VERT) in vec3 vertex;

out vec2 uv;

void main() {
    // Pass the vertices directly through
    gl_Position = vec4(vertex, 1);
    // Calculate the UV
    uv = (vertex.xy + vec2(1,1)) / 2.0;
}