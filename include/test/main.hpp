/*!
 *  @file include/test/main.hpp
 * 
 *  Main header file for the silhouette engine test implementation.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _TEST_MAIN_H_
#define _TEST_MAIN_H_

/*!
 *  Main method of the application.
 * 
 *  I only bothered writing documentation for this to satiate Doxygen.
 */
int main(int argc, char* argv[]);

#endif