/*!
 *  @file include/util/perf.hpp
 * 
 *  Static performance analysis system.  This basic utility implements a "tick"
 *  "tock" system for analyzing the performance of an application by measuring
 *  the time between two points in code.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _UTIL_PERF_H_
#define _UTIL_PERF_H_

#include <chrono>

#include "util/log.hpp"

/*!
 *  Global performance clock.
 * 
 *  This clock forms the basis of the performance analysys system.  This value
 *  is reset to the current time by `TICK` operations, and used to calculate the
 *  passage of time by `TOCK` operations.
 */
auto global_performance_clock = std::chrono::high_resolution_clock::now();

/*!
 *  Tick.
 * 
 *  Resets the global performance clock to zero (the current time).
 */
#define TICK global_performance_clock = std::chrono::high_resolution_clock::now();

/*!
 *  Tock.
 * 
 *  Calculates and prints the amount of time passed since the last `TICK`.
 */
#define TOCK { \
    auto duration = std::chrono::high_resolution_clock::now() - global_performance_clock; \
    float ms = duration.count() / 1000000.0; \
    DEBUG("Duration: [%uns] [%fms]", duration.count(), ms); \
}

/*!
 *  TickTock.
 * 
 *  Combines tick and tock into a single statement.
 */
#define TTOCK TICK TOCK

#endif
