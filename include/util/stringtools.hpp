/*!
 *  @file include/util/stringtools.hpp
 * 
 *  Utilities for dealing with c strings.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _UTIL_STRINGTOOLS_H_
#define _UTIL_STRINGTOOLS_H_

namespace util::string {

    /*!
     *  Build and allocate memory for a formatted string.
     * 
     *  This function operates similar to `strprintf`, except it automatically
     *  handles memory allocation and null termination.
     * 
     *  If you don't call `delete[]` on the result of this functino at some
     *  point, god will kill a puppy.
     * 
     *  @param  fmt     Message Format.
     *  @param  ...     `sprintf` style arguments.
     * 
     *  @return Pointer to a `char*` containing the formatted string.
     */
    char* format(const char* fmt, ...);

}

#endif