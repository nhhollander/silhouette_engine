/*!
 *  @file include/util/config.hpp
 * 
 *  This file defines the Configuration object and it's helper functions.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _UTIL_CONFIG_H_
#define _UTIL_CONFIG_H_

#include <unordered_map>
#include <vector>
#include <functional>

#define CV_WRITE_LOCK_NONE 0
#define CV_WRITE_LOCK_NEXTWRITE 1
#define CV_WRITE_LOCK_LOCKED 2

namespace util {

    // Forward declarations
    class Configuration;
    class ConfigurationValue;

    // Configuration change handler
    typedef std::function<void(util::ConfigurationValue*,util::Configuration*)> ConfigChangeHandler;

    /*!
     *  Configuration Value Container.
     * 
     *  Computers nowadays have plenty of ram, so keeping a copy of every value
     *  in a multitude of formats is a small price to pay.
     * 
     *  Configuration value containers are created once, and live in the same
     *  location for the entire lifespan of their parent Configuration object.
     *  Because of this, it is safe to perform a single lookup on a
     *  configuration and store the pointer for fast lookups at a later time.
     */
    class ConfigurationValue {

        private:

            /// Parent Configuration
            util::Configuration* parent;

            /// Change handlers
            std::vector<ConfigChangeHandler> change_handlers;

            /// Invoke change handlers
            void invoke_change_handlers();

            /*!
             *  Write Lock.
             * 
             *  **Warning: This is not a *secure* lock, it is intended to
             *  prevent accidental changes to values which are logically
             *  constant.**  For example, the silhouette engine value
             *  "window.dimx" is used to create frame buffers and texture, but
             *  no mechanism is or likely will be implemented to automatically
             *  re-initialize when the value is changed.  Because of this, there
             *  is no legitimate situation where a change to this value would
             *  lead to anything other than unexpected behavior.
             * 
             *  The write lock prevents the value of this configuration value
             *  from being changed accidentally.
             * 
             *  The following values may be used:
             *  | Value                   | Description                        |
             *  |-------------------------|------------------------------------|
             *  |`CV_WRITE_LOCK_NONE`     | All writes allowed.                |
             *  |`CV_WRITE_LOCK_NEXTWRITE`| Unlocked, will lock on next `set()`|
             *  |`CV_WRITE_LOCK_LOCKED`   | All writes will fail.              |
             */
            uint8_t write_lock = 0;

        public:

            /// Reference Name
            const char* ref_name = nullptr;

            /// Value as a string
            volatile char* string_ = nullptr;
            /// Value as an integer
            volatile int int_;
            /// Value as a double
            volatile double double_;
            /// Value as a boolean
            volatile bool bool_;

            /// Contains a valid boolean
            volatile bool valid_bool = false;

            /// Set the value from a string
            void set(const char* src);
            /// Set the value from an integer
            void set(int src);
            /// Set the value from a double
            void set(double src);
            /// Set the value from a boolean
            void set(bool src);

            /// Lock this configuration value
            void lock();
            /// Get the lock status
            int lock_status();

            /// Add a change handler
            void add_change_handler(ConfigChangeHandler handler);

            /// Configuration value constructor
            ConfigurationValue(util::Configuration* parent, const char* ref_name = "<unnamed>");
            /// Configuration value destructor
            ~ConfigurationValue();

    };
    
    /*!
     *  Configuration Class.
     * 
     *  The configuration class facilitates configurations and... uh... hmmm.
     * 
     *  I'm going to need to come back and write some more documentation for
     *  this.  If you're reading this are you aren't Nick, go bother him about  
     *  it.
     * 
     *  @include util/config.cpp
     */
    class Configuration {

        private:

            /*!
             *  Configuration Name.
             * 
             *  Each configuration should have a name associated with it to
             *  assist in the debugging of improperly formatted configuration
             *  entries.
             */
            const char* name;

        public:

            /*!
             *  Configuration Map.
             * 
             *  Entries are stored in pairs of uint32_t hashes generated with
             *  jenkins one-at-a-time hasing function, and the super-duper
             *  fancy-pantsy configuration container defined above.
             */
            std::unordered_map<uint32_t, ConfigurationValue*> config_values;

            /*!
             *  Create a new configuration.
             * 
             *  Instantiates a brand spankin' new configuration object, ready
             *  to have values loaded into it.
             * 
             *  @param name Name Optional name to assign to the configuration
             *                   file for use in debugging and error messages.
             */
            Configuration(const char* name = "<unnamed>");

            /*!
             *  Destroy the configuration object.
             * 
             *  Releases all memory allocated for the configuration object from
             *  memory.
             */
            ~Configuration();

            /*!
             *  Parse configuration data.
             * 
             *  This method parses and loads the given raw configuration data.
             *  Improperly formatted entries will generate a warning, and be
             *  ignored.
             * 
             *  @param cfgdata  Configuration data to load
             * 
             *  @return The number of entries loaded.
             */
            int parse(const char* cfgdata);

            /*!
             *  Load a configuration from a file.
             * 
             *  This method reads data from the specified file, and passes it to
             *  the `parse()` method for loading.
             * 
             *  @param fname    Name of the file to parse
             * 
             *  @return The number of entities loaded, or a negative number if
             *          an error prevented the file from being read.
             */
            int load(const char* fname);

            /*!
             *  Get a configuration value.
             * 
             *  This method attempts to locate and return a pointer to the
             *  ConfigurationValue structure for the requested key.
             * 
             *  In the event that no value can be located for the given key,
             *  this method will return a null pointer.
             * 
             *  @param key  Key to be hashed and used for the lookup
             *  @param quiet    Suppress warnings.
             * 
             *  @return Pointer to the configuration value, or nullptr if it can
             *          not be located.
             */
            ConfigurationValue* get(const char* key, bool quiet = false);

            /*!
             *  Get a string pointer.
             * 
             *  This method returns a pointer to the requested configuration
             *  value as a string.
             * 
             *  In the event that no value can be located for the given key,
             *  the optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return Pointer to the value, or `default_`
             */
            const volatile char** get_stringp(const char* key, const char** default__ = nullptr);

            /*!
             *  Get an integer pointer.
             * 
             *  This method returns a pointer to the requested configuration
             *  value as an integer.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return Pointer to the value, or `default_`
             */
            const volatile int* get_intp(const char* key, int* default_ = nullptr);

            /*!
             *  Get a double pointer.
             * 
             *  This method returns a pointer to the requested configuration
             *  value as a double.
             * 
             *  In the even that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return Pointer to the value, or `default_`
             */
            const volatile double* get_doublep(const char* key, double* default_ = nullptr);

            /*!
             *  Get a boolean pointer.
             * 
             *  This method returns a pointer to the requested configuration
             *  value as a boolean.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  In the event that a boolean is requested, but it is marked as
             *  invalid in the configuration value, a warning will be generated
             *  and the default value will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return Pointer to the value, or `default_`
             */
            const volatile bool* get_boolp(const char* key, bool* default_ = nullptr);

            /*!
             *  Get a string.
             * 
             *  This method returns the requested configuration value as a
             *  string.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return The requested value, or `default_`
             */
            const char* get_string(const char* key, const char* default_ = nullptr);

            /*!
             *  Get an integer.
             * 
             *  This method returns the requested configuration value as an
             *  integer.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return The requested value, or `default_`
             */
            int get_int(const char* key, int default_ = 0);

            /*!
             *  Get a double.
             * 
             *  This method returns the requested configuration value as a
             *  double.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return The requested value, or `default_`
             */
            double get_double(const char* key, double default_ = 0.0);

            /*!
             *  Get a boolean.
             * 
             *  This method returns the requested configuration value as a
             *  boolean.
             * 
             *  In the event that no value can be located for the given key, the
             *  optional `default_` parameter will be returned.
             * 
             *  In the event that a boolean is requested, but it is marked as
             *  invalid in the configuration value, a warning will be generated
             *  and the default value will be returned.
             * 
             *  @param key  Lookup key
             * 
             *  @return The requested value, or `default_`
             */
            bool get_bool(const char* key, bool default_ = false);

            /*!
             *  Set a string.
             * 
             *  This method sets the specified configuration value to the given
             *  string.
             * 
             *  In the event that no value object can be located for the given
             *  key, a warning will be generated unless the create parameter is
             *  set to true.
             * 
             *  @param key      Lookup key
             *  @param value    Value to set
             * 
             *  @return `true` on insertion, `false` otherwise.
             */
            bool set(const char* key, const char* value, bool create = false);

            /*!
             *  Set an integer.
             * 
             *  This method sets the specified configuration value to the given
             *  integer.
             * 
             *  In the event that no value object can be located for the given
             *  key, a warning will be generated unless the create parameter is
             *  set to true.
             * 
             *  @param key      Lookup key
             *  @param value    Value to set
             * 
             *  @return `true` on insertion, `false` otherwise.
             */
            bool set(const char* key, int value, bool create = false);

            /*!
             *  Set a double.
             * 
             *  This method sets the specified configuration value to the given
             *  double.
             * 
             *  In the event that no value object can be located for the given
             *  key, a warning will be generated unless the create parameter is
             *  set to true.
             * 
             *  @param key      Lookup key
             *  @param value    Value to set
             * 
             *  @return `true` on insertion, `false` otherwise.
             */
            bool set(const char* key, double value, bool create = false);

            /*!
             *  Set a boolean.
             * 
             *  This method sets the specified configuration value to the given
             *  boolean.
             * 
             *  In the event that no value object can be located for the given
             *  key, a warning will be generated unless the create parameter is
             *  set to true.
             * 
             *  @param key      Lookup key
             *  @param value    Value to set
             * 
             *  @return `true` on insertion, `false` otherwise.
             */
            bool set(const char* key, bool value, bool create = false);

    };

}

#endif