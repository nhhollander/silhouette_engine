/*!
 *  @file include/se/logic/logic.hpp
 * 
 *  Logic thread management.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_LOGIC_LOGIC_
#define _SE_LOGIC_LOGIC_

#include "se/silhouette.hpp"

#include <thread>
#include <vector>

namespace se::logic {

    /*!
     *  Logic control class.
     *
     *  The logic control class is responsible for invoking logic events
     *  throughout the engine in a timely manner.  It consists of a strictly
     *  timed logic thread, which invokes all registerd logic handlers at a
     *  given interval.
     * 
     *  It also keeps a scaled clock which increments its time based on a given
     *  time scale factor.  This allows for the passage of time within the
     *  engine to be changed on the fly.
     * 
     *  Because this thread must be able to run consistently, it's important
     *  that logic handlers complete their execution as soon as possible.
     */
    class Logic {

        private:

            /*!
             *  Logical Objects.
             * 
             *  This vector contains all currently registered logic handlers.
             *  Every logic tick every handler in this vector is invoked.
             */
            std::vector<se::logic::LogicHandler*> logic_objects;

            /*!
             *  Logic Thread.
             */
            std::thread logic_thread;

            /*!
             *  Tick Time.
             * 
             *  Number of nanoseconds per engine tick.
             */
            unsigned int tick_time;

            /*!
             *  Stop Flag.
             * 
             *  When set to `true`, the logic thread will terminate quickly.
             */
            volatile bool stop_flag = false;

            /*!
             *  Logic thread main method.
             */
            void logic_thread_main();

        public:

            /*!
             *  Construct a logic handler.
             */
            Logic();

            /*!
             *  Initialize the logic handler.
             * 
             *  This method initializes the logic thread.
             * 
             *  @return `true` on initialization success, `false` on failure.
             */
            bool init();

            /*!
             *  Stop the logic thread.
             * 
             *  This method sets the stop flag instructing the logic thread to
             *  terminate.
             */
            void stop_thread();

            /*!
             *  Register a logical object.
             *
             *  This method inserts the given object into the logical object
             *  queue, and will have its `logic_event()` method called every
             *  tick.
             *
             *  @param handler  Handler to register
             */
            void register_object(se::logic::LogicHandler* handler);

            /*!
             *  Deregister a logical object.
             * 
             *  This method removes the given object from the queue.
             * 
             *  @param  handler Handler to deregister
             * 
             *  @return `true` on removal, `false` if the object was not found.
             */
            bool deregister_object(se::logic::LogicHandler* handler);

            /*!
             *  Engine Time.
             * 
             *  This variable represents the current time in the engine.
             */
            uint64_t time = 0;

            /*!
             *  Time Scale.
             * 
             *  A value of 1.0 is real time, 0.5 is half time, 2.0 double etc.
             */
            const volatile double* time_scale;
    };

}

#endif