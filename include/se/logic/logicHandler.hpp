/*!
 *  @file include/se/logic/logicHandler.hpp
 * 
 *  Handler for objects capable of logical processing.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_LOGIC_LOGICHANDLER_H_
#define _SE_LOGIC_LOGICHANDLER_H_

namespace se::logic {

    /*!
     *  Logic handler abstract class.
     * 
     *  TODO: Documentation
     */
    class LogicHandler {

        public:

            /*!
             *  Logic Event.
             * 
             *  This method is called every time the engine processes a logic
             *  update.
             * 
             *  The time values are scaled by the current engine scale factor,
             *  and are therefore not gaurenteed to run coincidental with the
             *  actual passage of time.  All in-engine events should
             *  *exclusively* use these values to determine the passage of time.
             * 
             *  @param time     Elapsed time since last tick, in nanoseconds
             *  @param time     Scaled engine time, in nanoseconds
             */
            virtual void logic_event(uint32_t elapsed, uint64_t time) = 0;
    };

}

#endif