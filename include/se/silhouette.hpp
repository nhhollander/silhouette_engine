/*!
 *  @file include/se/silhouette.hpp
 * 
 *  Main silhouette engine file.  Contains forward declarations, incomplete
 *  types, and other headers used by pretty much everything in the engine.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_SILHOUETTE_H_
#define _SE_SILHOUETTE_H_

// Default values for invalid configuration states
#define SE_DEFAULT_VIDEO_WIDTH 640
#define SE_DEFAULT_VIDEO_HEIGHT 480
#define SE_DEFAULT_FPS_CAP 120
#define SE_DEFAULT_TPS 150
#define SE_DEFAULT_TIMESCALE 1.0

// Shader parameter locations
#define SE_SHADER_LOC_TEX_POS        1
#define SE_SHADER_LOC_TEX_NORM       2
#define SE_SHADER_LOC_TEX_ALB_SPEC   3
#define SE_SHADER_LOC_TEX_DEPTH      4
#define SE_SHADER_LOC_TEX_SCRTEX     5
#define SE_SHADER_LOC_TEX_ALBEDO     6
#define SE_SHADER_LOC_TEX_SPECULAR   7
#define SE_SHADER_LOC_IN_VERT        8
#define SE_SHADER_LOC_IN_UV          9
#define SE_SHADER_LOC_IN_NORM       10
#define SE_SHADER_LOC_IN_MVP        11
#define SE_SHADER_LOC_IN_DYN_LIGHT_COUNT    12
#define SE_SHADER_LOC_IN_MODEL_MAT  13
#define SE_SHADER_LOC_IN_CAM_POS    14

/// Position of the position texture color attachment
#define SE_SHADER_LOC_OUT_POS        0
/// Position of the normal texture color attachment
#define SE_SHADER_LOC_OUT_NORM       1
/// Position of the albedo/specular texture color attachment
#define SE_SHADER_LOC_OUT_ALB_SPEC   2
/// Position of the plain color texture color attachment (lighting final)
#define SE_SHADER_LOC_OUT_COLOR      0
                                

// OpenGL parameters
#define SE_DEFAULT_OPENGL_VERSION_MAJOR 4
#define SE_DEFAULT_OPENGL_VERSION_MINOR 3

// Forward declarations of engine types
namespace se {
    class Engine;
    class Entity;
    extern se::Engine* engine;
    namespace render {
        class Camera;
        class Light;
        class Mesh;
        class OpenGL;
        class Point3D;
        class Program;
        class Scene;
        class Screen;
        class Texture;
        struct CameraProperties;
        namespace program {
            class StaticPropShader;
            class LightingProgram;
        }
    }
    namespace entity {
        class StaticProp;
    }
    namespace logic {
        class LogicHandler;
    }
    namespace input {
        class KeyHandler;
        class MouseHandler;
    }
}

// Forward declarations of GL types
typedef int GLint;
typedef unsigned int GLuint;
typedef char GLchar;

/* Define the silhouette internal access specifier.
 *
 * Technically this is a violation of the C++ specification, but it adds an
 * important feature that c++ just doesn't have by default, effectively creating
 * a 'package private'.
 */
#ifdef SE_INTERNAL_BUILD
    #define se_internal public
#else
    #define se_internal private
#endif

#endif