/*!
 *  @file include/se/input/input.hpp
 * 
 *  Input wrapping and handling.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_INPUT_INPUT_H_
#define _SE_INPUT_INPUT_H_

#include "se/silhouette.hpp"

#include <thread>
#include <vector>
#include <unordered_map>
/* TODO: Is there a way to forward declare the SDL_Event and SDL_Keycode values
 *       including the entire header?  Am I being ridiculous in thinking there's
 *       any sort of benefit to optimizing this? */
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_events.h>

namespace se::input {

    /*!
     *  Input Management Class.
     * 
     *  This class is responsible for running the input processing thread,
     *  including managing input event assignments and dispatching events.
     */
    class Input {

        private:

            /*!
             *  Key Handler Map.
             * 
             *  Contains mappings between keys and `KeyHandler`s.
             */
            std::unordered_map<SDL_Keycode, std::vector<se::input::KeyHandler*>> key_handler_map;

            /*!
             *  Mouse Handler List.
             * 
             *  Contains a list of registered mouse event handlers.
             */
            std::vector<se::input::MouseHandler*> mouse_handlers;

            /*!
             *  Input Thread.
             */
            std::thread input_thread;

            /*!
             *  Thread stop flag.
             * 
             *  Setting to `true` indicates that the input thread should
             *  terminate.
             */
            volatile bool stop_flag = false;

            /*!
             *  Key Input Handler.
             * 
             *  This method handles keyboard inputs and passes them to their
             *  registered handlers.
             * 
             *  @param  event   SDL Key event
             */
            void handle_key_event(SDL_KeyboardEvent* event);

            /*!
             *  Mouse Input Handler.
             * 
             *  This method handles mouse inputs and passes them to their
             *  registered handlers.
             * 
             *  @param event    SDL Mouse event
             */
            void handle_mouse_event(SDL_MouseMotionEvent* event);

            /*!
             *  Input Thread main method.
             * 
             *  **This is the main method for the input thread**
             */
            void input_thread_main();

        public:

            /*!
             *  Construct an input handler.
             */
            Input();

            /*!
             *  Destroy the input handler.
             */
            ~Input();

            /*!
             *  Initialize the input handler.
             * 
             *  This method initializes the input handler thread and SDL's
             *  magical input stuff.
             * 
             *  @return `true` on initialization success, `false` on failure.
             */
            bool init();

            /*!
             *  Stop the input thread.
             * 
             *  This method sets the stop flag instructing the input thread to
             *  terminate.
             */
            void stop_thread();

            /*!
             *  Register a key handler.
             * 
             *  This method assigns the specified hander to the respective key
             *  event queue.
             * 
             *  @param  handler The handler to assign
             *  @param  key     The key to assign the handler to
             */
            void register_key_handler(KeyHandler* handler, SDL_Keycode key);

            /*!
             *  Deregister a key handler.
             * 
             *  This method removes the specified handler from the given key
             *  event queue.
             * 
             *  If the specified key handler is not registered to this input
             *  manager, this method will return `false`.
             * 
             *  @param  handler The handler to evict
             *  @param  key     The key handler to evict from
             * 
             *  @return `true` on removal, `false` if the given handler was
             *          never registered.
             */
            bool deregister_key_handler(KeyHandler* handler, SDL_Keycode key);

            /*!
             *  Register a mouse handler.
             * 
             *  This method inserts the specified handler to the mouse event
             *  handler queue.
             * 
             *  @param handler  The handler to assign
             */
            void register_mouse_handler(MouseHandler* handler);

            /*!
             *  Deregister a mouse handler.
             * 
             *  This method removes the specified handler from the mouse event
             *  handler queue.
             * 
             *  If the specified mouse handler is not registered to this input
             *  manager, this method will return `false`.
             * 
             *  @param handler  The handler to remove
             * 
             *  @return `true` on removal, `false` if the given handler was
             *          never registered.
             */
            bool deregister_mouse_handler(MouseHandler* handler);

    };
}

#endif