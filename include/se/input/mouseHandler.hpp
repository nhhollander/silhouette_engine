/*!
 *  @file include/se/input/mouseHandler.hpp
 * 
 *  Mouse input event handling.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_INPUT_MOUSEHANDLER_H_
#define _SE_INPUT_MOUSEHANDLER_H_

#include <SDL2/SDL_events.h>

namespace se::input {

    /*!
     *  Mouse handler abstract class.
     * 
     *  This class provides a template for classes that need the ability to
     *  respond to a mouse press event.
     */
    class MouseHandler {

        public:

            /*!
             *  Mouse Event.
             * 
             *  This method is called every time the mouse does a thing.
             * 
             *  @param event    The SDL mouse event object.
             */
            virtual void mouse_event(SDL_MouseMotionEvent* event) = 0;

    };

}

#endif