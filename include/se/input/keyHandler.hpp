/*!
 *  @file include/se/input/keyHandler.hpp
 * 
 *  Key input event handling.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_INPUT_KEYHANDLER_H_
#define _SE_INPUT_KEYHANDLER_H_

#include <SDL2/SDL_events.h>

namespace se::input {

    /*!
     *  Input handler abstract class.
     * 
     *  This class provides a template for classes that need the ability to
     *  respond to a key press event.
     */
    class KeyHandler {

        public:

            /*!
             *  Key Event.
             * 
             *  This method is called every time a selected key is pressed *OR*
             *  released.
             * 
             *  @param event    The SDL key event object.
             */
            virtual void key_event(SDL_KeyboardEvent* event) = 0;

    };

}

#endif