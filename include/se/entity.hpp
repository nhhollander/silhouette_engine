/*!
 *  @file include/se/entity.hpp
 * 
 *  Definition of the silhouette engine's Entity class.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_ENTITY_H_
#define _SE_ENTITY_H_

#include "se/silhouette.hpp"
#include "se/render/point3d.hpp"

namespace se {

    /*!
     *  Entity Class.
     * 
     *  The entity class represents a single "thing" that exists in the engine.
     *  Entities can be renderable objects, physics objects, game control
     *  objects, or any number of other discrete items, so long as those items
     *  exist in the game world.
     */
    class Entity : public se::render::Point3D {

        private:

        public:

            /*!
             *  Entity Constructor.
             */
            Entity();

            /*!
             *  Entity Destructor.
             */
            ~Entity();

            /*!
             *  Draw this entity.
             * 
             *  This virtual function should be overridden by the entity with
             *  some OpenGL code.
             * 
             *  **Note: This method must only be called from the render thread.
             *  Invocations from other threads may lead to application
             *  instability or crashes**
             */
            virtual void draw(se::render::CameraProperties* properties) = 0;

    };

}

#endif