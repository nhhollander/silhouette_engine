/*!
 *  @file include/se/engine.hpp
 * 
 *  Definition of the silhouette engine's Engine class.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_ENGINE_H_
#define _SE_ENGINE_H_

#include "se/silhouette.hpp"

#include "se/input/input.hpp"
#include "se/logic/logic.hpp"
#include "se/render/opengl.hpp"
#include "util/config.hpp"

/*!
 *  Silhouette Engine Namespace.
 * 
 *  All classes and functions related to the silhouette engine exist within this
 *  namespace.
 */
namespace se {

    /*!
     *  Silhouette Engine Class.
     * 
     *  This is the main class for an instance of the silhouette engine.  All
     *  activities related to an instance of the engine take place relative to
     *  this object.
     */
    class Engine {

        private:

            /*!
             *  Stop Flag.
             * 
             *  When set to `true` all child threads will be joined.
             */
            volatile bool stop_flag = false;

        se_internal:

            /*!
             *  OpenGL Instance.
             * 
             *  This wrapper is responsible for for the render thread and
             *  render related functions.
             */
            se::render::OpenGL opengl = se::render::OpenGL();

            /*!
             *  Input Manager.
             */
            se::input::Input input = se::input::Input();

            /*!
             *  Logic Manager.
             */
            se::logic::Logic logic = se::logic::Logic();

        public:

            /*!
             *  Primary Engine Configuration.
             * 
             *  This config contains all settings for the engine and it's
             *  submodules.
             * 
             *  TODO: Write additional documentation for this variable
             */
            util::Configuration config = util::Configuration("Engine");

            /*!
             *  Engine Constructor.
             * 
             *  Creates a new instance of the Silhouette Engine.
             * 
             *  If this is the first instance of an engine created, a pointer
             *  will be saved to `se::default_engine`.
             * 
             *  TODO: Document initial allocation behavior in greater detail.
             */
            Engine();

            /*!
             *  Engine Destructor.
             * 
             *  This method ensures that all memory and resources allocated by
             *  the engine are freed.
             * 
             *  TODO: Document de-allocation behavior in greater detail.
             */
            ~Engine();

            /*!
             *  Initialize the engine.
             * 
             *  This method initializes the engine's various subsystems,
             *  returning `false` immediately if one of the subsystems fails
             *  to initialize.
             * 
             *  @return `true` if initialization is successful, `false` on error
             */
            bool init();

            /*!
             *  Quit the engine.
             * 
             *  This method instructs all submodules to clean up and die.
             */
            void quit();

            /*!
             *  Wait for quit.
             * 
             *  This method blocks until all engine threads are terminated.
             */
            void wait_for_quit();

    };

    /*!
     *  Singular Engine Instance.
     * 
     *  Pointer to the engine instance.  Any submodule requiring access to the
     *  engine should use this variable.
     * 
     *  ### Implementation Note
     * 
     *  To drastically simplify the implementation of this engine, I have made
     *  the somewhate regrettable decision to utilize a singleton for the
     *  engine.  Is this bad practice?  Technically yes, but sometimes a single
     *  "bad" decision can save hours of debugging and implementation
     *  difficulties.
     * 
     *  By replacing the original `default_engine` system with this, and
     *  removing the ability to construct objects for multiple simulaneous
     *  engine instnaces, the odds of royally screwing something up are
     *  significantly lower.
     */
    extern se::Engine* engine;

}

#endif