/*!
 *  @file include/se/entity/player.hpp
 * 
 *  Player Controllable Entity.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_ENTITY_PLAYERCAMERA_H_
#define _SE_ENTITY_PLAYERCAMERA_H_

#include "se/engine.hpp"
#include "se/input/keyHandler.hpp"
#include "se/input/mouseHandler.hpp"
#include "se/logic/logicHandler.hpp"
#include "se/render/camera.hpp"

namespace se::entity {

    /*!
     *  Player Camera Entity.
     * 
     *  The player camera is a basic psudeoentity that provides first-person
     *  controls to a regular camera object.
     */
    class PlayerCamera :
        public se::render::Camera,
        public se::input::KeyHandler,
        public se::input::MouseHandler,
        public se::logic::LogicHandler {

        private:

            /*!
             *  Engine Reference.
             */
            se::Engine* engine;

            /// State of the W key
            volatile bool key_w = false;
            /// State of the S key
            volatile bool key_s = false;
            /// State of the A key
            volatile bool key_a = false;
            /// State of the D key
            volatile bool key_d = false;

        public:

            /*!
             *  Construct a camera.
             * 
             *  Creates a new PlayerCamera object.
             * 
             *  @param  width   Width of the buffer
             *  @param  height  Height of the buffer
             */
            PlayerCamera(int width, int height);

            /*!
             *  Destroy the camera.
             */
            ~PlayerCamera();

            /*!
             *  Handle a keyboard event.
             * 
             *  The camera uses these for motion calculations.
             * 
             *  @param  event   SDL event containing the keyboard event.
             */
            void key_event(SDL_KeyboardEvent* event);

            /*!
             *  Handle a mouse event.
             * 
             *  The camera uses these for updating camera rotation.
             * 
             *  @param  event   SDL event containing the mouse event.
             */
            void mouse_event(SDL_MouseMotionEvent* event);

            /*!
             *  Handle a logic tick.
             * 
             *  @param  elapsed Nanoseconds since last event (scaled)
             *  @param  time    Nanoseconds since program start (scaled)
             */
            void logic_event(uint32_t elapsed, uint64_t time);

    };

}

#endif