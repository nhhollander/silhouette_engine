/*!
 *  @file include/se/entity/staticProp.hpp
 * 
 *  Entity that renders a model.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_ENTITY_STATICPROP_H_
#define _SE_ENTITY_STATICPROP_H_

#include "se/silhouette.hpp"
#include "se/entity.hpp"

namespace se::entity {

    class StaticProp : public se::Entity{

        private:

            /*!
             *  Prop Mesh.
             * 
             *  The geometry of the prop that is to be drawn.
             */
            se::render::Mesh* mesh;

            /*!
             *  Prop Texture.
             * 
             *  The diffuse texture of the prop.
             */
            se::render::Texture* texture;

            /*!
             *  Static prop shader program.
             * 
             *  This is the program specifically designed and engineered to
             *  provide the worlds most advanced rendering of stupid stuff.
             */
            se::render::program::StaticPropShader* program = nullptr;

            /*!
             *  Name of the model file.
             */
            char* model_name;

            /*!
             *  Name of the texture file.
             */
            char* texture_name;

        public:

            /*!
             *  Construct a new static prop.
             * 
             *  @param  model   Name of the model file to load.
             *  @param  texture Name of the texture file to load.
             */
            StaticProp(const char* model, const char* texture);

            /*!
             *  Destroy an entity.
             */
            ~StaticProp();
            
            /*!
             *  Draw the prop.
             */
            void draw(se::render::CameraProperties* properties);
    };

}

#endif