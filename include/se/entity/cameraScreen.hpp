/*!
 *  @file include/se/entity/cameraScreen.hpp
 * 
 *  Entity that renders the view from a camera.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_ENTITY_CAMERA_SCREEN_H_
#define _SE_ENTITY_CAMERA_SCREEN_H_

#include "se/silhouette.hpp"
#include "se/entity.hpp"

namespace se::entity {

    /*!
     *  Camera Screen.
     * 
     *  Camera screens can be used to render the view from a camera object onto
     *  a plane.
     */
    class CameraScreen : public se::Entity {

        private:

            /*!
             *  Engine Reference.
             */
            se::Engine* engine;

            /*!
             *  Shader Program.
             */
            se::render::Program* program;


        public:

            /*!
             *  Create a Camera Screen.
             */
            CameraScreen();

            /*!
             *  Camera to render.
             * 
             *  Each time this entity is rendered, this camera will be used to
             *  generate its texture.
             */
            se::render::Camera* camera = nullptr;

            /*!
             *  Target Scene.
             */
            se::render::Scene* scene = nullptr;

            /*!
             *  Screen Mesh.
             */
            se::render::Mesh* mesh = nullptr;

            /*!
             *  Render the screen.
             * 
             *  This method renders a frame from the perspective of the camera
             *  and uses it to draw this.
             */
            void draw(se::render::CameraProperties* properties);

    };

}

#endif