/*!
 *  @file include/se/render/opengl.hpp
 * 
 *  OpenGL wrapper class definition and (maybe a couple of) utility methods.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_OPENGL_H_
#define _SE_RENDER_OPENGL_H_

#include "se/silhouette.hpp"

#include <queue>
#include <thread>
#include <unordered_map>
#include <functional>

#include "se/render/mesh.hpp" // TODO: This includes a lot - is it needed?

// Forward declaration of SDL objects
typedef struct SDL_Window SDL_Window;
typedef void *SDL_GLContext;

namespace se::render {

    /*!
     *  OpenGL wrapper and utility class.
     * 
     *  This class provides an object-oriented interface for the OpenGL graphics
     *  interface thing.
     */
    class OpenGL {

        private:

            /*!
             *  SDL2 OpenGL Context for drawing.
             * 
             *  This is the main graphics context for the application.  Whenever
             *  something is drawn to the screen, it's done through this bad boy
             *  right here.
             */
            SDL_GLContext glcontext;

            /*!
             *  Parent Window.
             * 
             *  Pointer to the window that should be used for opengl context
             *  and sufrace creation.
             */
            SDL_Window* window;

            /*!
             *  Render Thread.
             * 
             *  This thread contains all rendering actions, from compilation and
             *  linking of shaders, to performing screen rendering.
             */
            std::thread render_thread;

            /*!
             *  Initialization complete flag.
             * 
             *  For as long as this variable is `false`, the initialization
             *  phase is not yet complete.  The initialization result flag must
             *  be set before this one.
             */
            volatile bool init_complete = false;

            /*!
             *  Initialization failure flag.
             * 
             *  If the initialization process fails, this value must be set to
             *  `true` *before `init_complete` is set to `true`*.  It indicates
             *  that the init process has encountered a fatal an unrecoverable
             *  error, and the engine can not be used.
             */
            volatile bool init_failure = false;
            
            /*!
             *  Thread stop flag.
             * 
             *  Setting to true indicates that the render thread should
             *  terminate.
             */
            volatile bool stop_flag = false;

            /*!
             *  Special Render Operation flag.
             * 
             *  This flag, when set to true, indicates that there are special
             *  render operations such as shader compilation, program linking,
             *  mesh binding, etc.  Unless this flag is set, the render thread
             *  will not check for pending special operations.
             */
            volatile bool pending_operation = false;

            /*!
             *  Special Function Queue.
             * 
             *  Functions in this queue will be executed on at a time for each
             *  frame rendered.
             */
            std::queue<std::function<void()>> special_function_queue;

            /*!
             *  Active camera.
             */
            se::render::Camera* active_camera = nullptr;

            /*!
             *  Screen to use for rendering.
             */
            se::render::Screen* screen = nullptr;

            /*!
             *  Frame time.
             * 
             *  The frame time defines the minimum duration of a render cycle.
             *  It is defined as 1/<opengl.fps_cap>.  This value is calculated
             *  at runtime based on the configured FPS cap and
             *  CLOCKS_PER_SECOND, and therefore has no defined units.
             */
            unsigned int frame_time = 0;

            /*!
             *  Internal Initialization.
             * 
             *  **This method should only be called from the render thread**
             * 
             *  This method initializes the opengl instance.
             */
            void init_internal();

            /*!
             *  Render Frame.
             * 
             *  This is the magic method that makes it all happen!
             * 
             *  Iterate over visible entities, and render them to the screen.
             *  Currently all renderable entities added to the engine are drawn.
             *  
             *  Note: Currently the rendering process is very poorly optimized,
             *  and does not deliver even slightly acceptable performance.
             *  With some luck, this will get re-done soon enough, and a system
             *  for performing rendering at an acceptable rate will be complete.
             */
            void render_frame();

            /*!
             *  Render thread main method.
             * 
             *  **This is the main method for the OpenGL thread**
             */
            void render_thread_main();

        se_internal:

            /*!
             *  Initialize the OpenGL instance.
             * 
             *  This method initializes SDL, configures the window parameters,
             *  and creates the OpenGL context.
             *
             *  @return `true` on initialization success, `false` on failure.
             */
            bool init();

            /*!
             *  Stop the render thread.
             * 
             *  This method sets the render thread stop flag, which causes the
             *  renderer to exit on it's next loop.
             */
            void stop_thread();

            /*!
             *  Active Program.
             * 
             *  Points to the last shader program to be used.
             */
            se::render::Program* active_program = nullptr;

        public:

            /*!
             *  Instantiate a new OpenGL instance.
             * 
             *  This constructor instantiates the object, but does not
             *  initialize it or create the underlaying OpenGL instance.  In
             *  order to use this class, init() must be called.
             */
            OpenGL();

            /*!
             *  Destroy the OpenGL instance.
             * 
             *  This method de-allocates all resources, buffers, and objects
             *  that exist within this class.
             */
            ~OpenGL();

            /*!
             *  Set the active camera.
             * 
             *  TODO: Document this
             */
            bool set_camera(se::render::Camera* camera);

            /*!
             *  Insert a special function into the special function queue.
             * 
             *  Sertaion operations within the engine can only be performed on
             *  the render thread, such as the binding of textures or mesh data.
             *  This method takes a function object that will be invoked as
             *  quickly as possible.
             * 
             *  To prevent engine hangs, functions passed to this method should
             *  make an honest attempt to return as quickly as possible.
             * 
             *  @param func     Function to invoke from the render thread
             */
            void insert_special_function(std::function<void()> func);


    };

}

#endif