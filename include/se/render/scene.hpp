/*!
 *  @file include/se/render/scene.hpp
 * 
 *  Scene Definitions.  Scenes are basically fancy containers for geometry, but
 *  they're fancy, so they're better.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_SCENE_H_
#define _SE_RENDER_SCENE_H_

#include "se/silhouette.hpp"

#include <list>

namespace se::render {

    /*!
     *  Scene Class.
     * 
     *  Scenes are fancy containers for geometry.
     */
    class Scene {

        public:

            /*!
             *  Entities in the scene.
             * 
             *  This list contains all entities that could be drawn in the
             *  scene.
             */
            std::list<se::Entity*> entities;

            /*!
             *  Dynamic Lights.
             * 
             *  This list contains all dynamic lights that could be used to
             *  render the scene.
             */
            std::list<se::render::Light*> lights;

            /*!
             *  Load a scene from a scene file.
             * 
             *  @param  fname   Name of the scene description file.
             *  @param  engine  Engine to bind objects to.
             */
            void load_from_file(const char* fname, se::Engine* engine);

    };

}

#endif