/*!
 *  @file include/se/render/mesh.hpp
 * 
 *  Definition of an opengl 3d point.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_POINT3D_H_
#define _SE_RENDER_POINT3D_H_

#include "se/silhouette.hpp"

#include <glm/mat4x4.hpp>

namespace se::render {

    /*!
     *  3D Point template class.
     * 
     *  This class represents a point in model space.  Methods are available to
     *  generate the model and/or view matrices depending on how this class is
     *  used.
     */
    class Point3D {

        public:

            /*!
             *  Parent Point.
             */
            se::render::Point3D* parent = nullptr;

            /*!
             *  Parent translation.
             * 
             *  If set to true, the point will also apply its parent's
             *  translation.
             */
            bool parent_translation = true;

            /*!
             *  Parent rotation.
             * 
             *  If set to true, the point will also apply its parent's rotation.
             */
            bool parent_rotation = true;

            /*!
             *  Parent scale.
             * 
             *  If set to true, the point will also apply its parent's scale.
             */
            bool parent_scale = true;

            /*!
             *  Additive parenting.
             * 
             *  If additive parenting is enabled, insatead of multiplying this
             *  entities translation matrix by that of its parents, it will add
             *  the values of its parents translations to itself.
             */
            bool parent_additive_mode = false;

            /// X Position
            volatile float x = 0.0;
            /// Y Position
            volatile float y = 0.0;
            /// Z Position
            volatile float z = 0.0;

            /// X Rotation
            volatile float rx = 0.0;
            /// Y Rotation
            volatile float ry = 0.0;
            /// Z Rotation
            volatile float rz = 0.0;

            /// X Scale
            volatile float sx = 1.0;
            /// Y Scale
            volatile float sy = 1.0;
            /// Z Scale
            volatile float sz = 1.0;

            /*!
             *  Inversion Flag.
             * 
             *  When calculating matrices for cameras, all operations must be
             *  inverted.  This flag inverts the position, rotation, and scale
             *  values, reverses the order of the matrices calculated by
             *  get_matrix(), and flips the angle order used when calculating
             *  the rotation matrix.
             */
            volatile bool invert = false;

            /*!
             *  Generate the Translation Matrix.
             */
            glm::mat4 get_translation_matrix();

            /*!
             *  Generate the Rotation Matrix.
             * 
             *  Calculates the rotation matrix in ZXY order, unless the invert
             *  flag is set, in which case it returns in YXZ order.
             */
            glm::mat4 get_rotation_matrix();

            /*!
             *  Generate the Scale Matrix.
             */
            glm::mat4 get_scale_matrix();

            /*!
             *  Generate the Combined Matrix.
             */
            glm::mat4 get_matrix();

    };

}

#endif