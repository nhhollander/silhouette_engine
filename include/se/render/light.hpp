/*!
 *  @file include/se/render/light.hpp
 * 
 *  Dynamic light point
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_LIGHT_H_
#define _SE_RENDER_LIGHT_H_

#include "se/silhouette.hpp"

#include "se/render/point3d.hpp"

namespace se::render {

    /*!
     *  Dynamic Light.
     * 
     *  Specifies a point in 3d space that serves as the origin for a dynamic
     *  light.
     */
    class Light : public Point3D {

        private:

        public:

            /*!
             *  Color.
             */
            float color[3] = {1.0, 1.0, 1.0};

            /*!
             *  Light Constructor.
             */
            Light();

            /*!
             *  Light Constructor.
             * 
             *  Builds a dynamic light with pre-specified position and color.
             */
            Light(float x, float y, float z, float r, float g, float b);

    };

}

#endif