/*!
 *  @file include/se/render/program/staticPropShader.hpp
 * 
 *  Entity that renders a model.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_PROGRAM_STATICPROPSHADER_H_
#define _SE_RENDER_PROGRAM_STATICPROPSHADER_H_

#include "se/silhouette.hpp"
#include "se/render/program.hpp"

namespace se::render::program {

    /*!
     *  Static Prop Shader.
     * 
     *  This basic shader allows the rendering of static props at any specified
     *  location in the world, but does not permit animations or other dynamic
     *  features.
     */
    class StaticPropShader : public se::render::Program {

        public:
        
            /*!
             *  Create a new instance of a static prop shader.
             */
            StaticPropShader();

            /*!
             *  Get the name of this shader program.
             */
            const char* program_name();

            /*!
             *  Get a unique hash of this shader program.
             */
            uint32_t hash();

    };

}

#endif