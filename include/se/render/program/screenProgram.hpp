/*!
 *  @file include/se/render/program/screenProgram.hpp
 * 
 *  OpenGL shader program for rendering a square to the screen.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_PROGRAM_SCREENPROGRAM_H_
#define _SE_RENDER_PROGRAM_SCREENPROGRAM_H_

#include "se/silhouette.hpp"
#include "se/render/program.hpp"

namespace se::render::program {

    /*!
     *  Screen Drawing Program.
     * 
     *  This program is responsible for drawing a texture, usually from a
     *  framebuffer, to the computer display.
     */
    class ScreenProgram : public Program {

        private:

        public:

            /*!
             *  Construct a new screen program.
             */
            ScreenProgram();

            /*!
             *  Destroy this screen.
             */
            ~ScreenProgram();

            /*!
             *  Program Name.
             * 
             *  @return The name of this program.
             */
            const char* program_name();

            /*!
             *  Unique program hash.
             * 
             *  @return A unique hash.
             */
            uint32_t hash();

    };

}

#endif