/*!
 *  @file include/se/render/program/lightingProgram.hpp
 * 
 *  OpenGL shader program for rendering a square to the screen.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_PROGRAM_LIGHTINGPROGRAM_H_
#define _SE_RENDER_PROGRAM_LIGHTINGPROGRAM_H_

#include "se/silhouette.hpp"
#include "se/render/program.hpp"

namespace se::render::program {

    /*!
     *  Light Location Structure.
     * 
     *  Contains the uniform locations of the light properties to be passed in.
     */
    struct LightLocations {
        /// Location of the position
        GLuint position_loc;
        /// Location of the color
        GLuint color_loc;
    };

    /*!
     *  Lighting Calculation Program.
     * 
     *  This program is responsible for taking a series of deferred rendering
     *  buffers, and performing the lighting calculations required to generate a
     *  beautiful scene.
     */
    class LightingProgram : public Program {

        private:

            /*!
             *  Maximum number of dynamic lights.
             */
            int max_lights;

        se_internal:
        
            /*!
             *  Link Override.
             * 
             *  This method calls the base linker function, and then locates
             *  the uniform parameters.
             */
            virtual GLuint link_internal();

        public:

            /*!
             *  Construct a new lighting program.
             */
            LightingProgram();

            /*!
             *  Destroy this program.
             */
            ~LightingProgram();

            /*!
             *  Program Name.
             * 
             *  @return The name of this program.
             */
            const char* program_name();

            /*!
             *  Dynamic light locations.
             * 
             *  Determined dynamically during the linking process.
             */
            LightLocations* locations = nullptr;

            /*!
             *  Unique program hash.
             * 
             *  @return A unique hash.
             */
            uint32_t hash();

    };

}

#endif