/*!
 *  @file include/se/render/shader.hpp
 * 
 *  This file defines OpenGL shader loading, compilation, and caching utilities.
 * 
 *  Shaders are loaded from disk and compiled on request.  Most methods in this
 *  class must be called from the render thread to prevent the application from
 *  crashing.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_SHADER_H_
#define _SE_RENDER_SHADER_H_

#include "se/silhouette.hpp"

#include <unordered_map>

namespace se::render {

    /*!
     *  Shader module cache.
     * 
     *  This cahce contains compiled shader modules stored as key-value pairs,
     *  where the key is a jenkins hash of the shader ID, or shader source code
     *  if it was loaded from memory.
     */
    extern std::unordered_map<uint32_t, GLuint> shader_cache;

    /*!
     *  Get a shader.
     * 
     *  This function attempts to load and return the requested shader, first
     *  looking in the shader cache.  If the shader is not in the cache, it is
     *  loaded from disk and compiled.
     * 
     *  The "name" parameter must correspond with a shader source file located
     *  in <app.datafolder>/shaders.  The name of the file must contain ".vert"
     *  or ".vs" for vertex shaders, and ".frag" or ".fs" for fragment shaders.
     *  Other file extensions are not permitted, and will generate an error.
     * 
     *  **Note:** This function must be called from the render thread.
     * 
     *  @param  name    The name of the shader to load
     *  @param  defines [Optional] Extra defines to use while compiling
     * 
     *  @return The shader ID.
     */
    GLuint load_shader(const char* name, const char* defines = nullptr);

    /*!
     *  Compile a shader from memory.
     * 
     *  This function functions similar to load_shader, however it is intended
     *  to take already loaded shader code as an argument.
     * 
     *  **Note:** This function must be called from the render thread.
     * 
     *  @param  source  The source code of the shader to load
     *  @param  type    The shader type
     * 
     *  @return The shader ID.
     */
    GLuint load_shader(const char* source, GLuint type);

}

#endif
