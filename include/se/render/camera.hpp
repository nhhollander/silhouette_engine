/*!
 *  @file include/se/render/camera.hpp
 * 
 *  Camera Object Definitions.
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_CAMERA_H_
#define _SE_RENDER_CAMERA_H_

#include <glm/mat4x4.hpp>

#include "se/silhouette.hpp"
#include "se/render/point3d.hpp"

namespace se::render {

    /*!
     *  Camera Properties Structure.
     * 
     *  This structure contains a bunch of properties that describe how
     *  entities, meshes, and shaders should behave when drawing.
     * 
     *  This structure contains properties that describe how entities, meshes,
     *  and shaders should behave when drawing.  It also contains information
     *  about the current render pass, including the number of nested cameras
     *  currently rendering, and the target frame buffer ID.
     */
    struct CameraProperties {
        /*!
         *  Camera translation matrix.
         * 
         *  This matrix contains the combination of the view and projection
         *  matrices.
         */
        glm::mat4 camera_matrix;

        /*!
         *  Camera depth.
         * 
         *  This value reflects the number of nested camera views that are being
         *  rendered.
         */
        int camera_depth = 0;

        /*!
         *  Previous camera buffer.
         * 
         *  This is the ID of the camera that is currently rendering.  Entities
         *  should ensure this is the bound framebuffer before drawing geometry.
         * 
         *  This value MUST always be restored as the active frame buffer at the
         *  end of the render method for any entities that change the active
         *  frame buffer as part of their rendering process.
         */
        GLuint framebuffer_id;
    };

    /*!
     *  Camera Class.
     * 
     *  At the most basic level, a Camera is a system that can render the
     *  contents of a scene to an OpenGL texture, and make that texture
     *  available for rendering by another Camera or Screen.
     */
    class Camera : public Point3D {

        private:

            /*!
             *  Configuration Status.
             * 
             *  0 = Not Configured
             *  1 = Configuring
             *  2 = Configured
             *  3 = Failure
             */
            int config_status = 0;

            /*!
             *  Deferred Render Position Color Buffer.
             */
            GLuint position_buffer;

            /*!
             *  Deferred Render Normal Color Buffer.
             */
            GLuint normal_buffer;

            /*!
             *  Deferred Render Albedo/Specular Color Buffer.
             */
            GLuint albedo_specular_buffer;

            /*!
             *  Deferred Render Depth (Texture) Buffer.
             */
            GLuint depth_texture;

            /*!
             *  Deferred Render Depth Buffer.
             */
            GLuint depth_buffer;

            /*!
             *  G-Buffer.
             * 
             *  The G-Buffer is used for deferred rendering, and collects all
             *  textures required for the deferred lighting pass.
             */
            GLuint g_buffer = -1;

            /*!
             *  Lighting Buffer.
             * 
             *  The lighting buffer is used to take inputs from the deferred
             *  rendering process, and apply lighting.
             */
            GLuint lighting_buffer = -1;

            /*!
             *  Result Buffer.
             * 
             *  The result of the lighting pass.
             */
            GLuint result_buffer;

            /*!
             *  Screen Mesh.
             * 
             *  Triangle geometry used for the lighting render.
             */
            se::render::Mesh* screen_mesh;

            /*!
             *  Lighting Program.
             */
            se::render::program::LightingProgram* lighting_program;

            /*!
             *  SuperSampling factor.
             * 
             *  If set to 1, supersampling is disabled.
             */
            int supersample = 1;

            /// Buffer width
            int width;
            /// Buffer height;
            int height;

            /// Field of view (FoV)
            float fov = 1.2;

            /*!
             *  Configure the buffers.
             * 
             *  Sets up the texture, frame buffer, and depth buffer required for
             *  rendering.
             */
            void configure_buffers();

        public:

            /*!
             *  DEBUG! CAMERA MODE!
             * 
             *  For testing purposes, should be removed in the near future.
             */
            int camera_mode = 2;

            /// Wireframe mode
            bool wireframe = false;

            /// Near clipping plane
            float clip_near = 0.1;
            /// Far clipping plane
            float clip_far = 100.0;

            /// Default scene
            se::render::Scene* default_scene = nullptr;

            /*!
             *  Render a frame.
             * 
             *  @param scene        The scene to render with the camera
             *  @param properties   Optional properties for a nested camera.
             */
            void render(se::render::Scene* scene = nullptr, se::render::CameraProperties* properties = nullptr);

            /*!
             *  Get the camera texture ID.
             * 
             *  This returns the OpenGL texture ID for this camera's buffer.
             */
            GLuint get_texture_id();

            /*!
             *  Get supersample factor.
             */
            int ss_factor();

            /*!
             *  Constructor.
             * 
             *  @param width    Width of the buffer
             *  @param height   height of the buffer
             */
            Camera(int width, int height);

    };

}

#endif