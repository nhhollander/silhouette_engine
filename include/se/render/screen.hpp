/*!
 *  @file include/se/render/screen.hpp
 * 
 *  Screens are used for drawing textures to the screen.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_SCREEN_H_
#define _SE_RENDER_SCREEN_H_

#include "se/silhouette.hpp"

namespace se::render {

    /*!
     *  Screen Renderer.
     * 
     *  Screen renderers take a texture, and draw it to the screen.
     */
    class Screen {

        private:

            /*!
             *  Engine reference.
             */
            se::Engine* engine;

            /*!
             *  Screen Geometry.
             * 
             *  Everyting in opengl must be drawn as geometry, including this
             *  screen.  We don't need anything fancy, so it's just going to be
             *  two triangles that cover the whole thing.
             */
            se::render::Mesh* screen_geometry;

            /*!
             *  Screen Program.
             */
            se::render::Program* screen_program;

        public:

            /*!
             *  Create a new screen.
             */
            Screen();

            /*!
             *  Destroy the screen.
             */
            ~Screen();

            /*!
             *  Render this screen.
             * 
             *  Updates the camera then draws the resulting buffer to the
             *  screen.
             * 
             *  @param camera   Camera to use to render the scene
             *  @param scene    Scene to render
             */
            void render(se::render::Camera* camera, se::render::Scene* scene);
            
    };

}

#endif
