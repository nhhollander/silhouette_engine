/*!
 *  @file include/se/render/mesh.hpp
 * 
 *  Definition of the renderable mesh class.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_MESH_H_
#define _SE_RENDER_MESH_H_

#include "se/silhouette.hpp"

#include <cstdint>
#include <unordered_map>

namespace se::render {

    /*!
     *  Mesh data structure.
     * 
     *  Contains pointers to the raw data that makes up this mesh.
     */
    struct MeshData {
        /// Vertex Data
        float* vertex_data = nullptr;
        /// UV Data
        float* uv_data = nullptr;
        /// Normal Data
        float* normal_data = nullptr;
        /*!
         *  Vertex Count.
         * 
         *  **Warning: This value is only applicable for the mesh object that
         *  loads the model data.  It should not be used anytime after the
         *  binding process, as it may not be set correctly for all instances of
         *  an object.**
         */
        int vertex_count = -1;
    };

    /*!
     *  OpenGL data.
     * 
     *  This structure contains all of the shared opengl data.  Values may be
     *  set to -1 even when the opengl specification would normally prohibit it
     *  to specify that they are unset.  For example, opengl object IDs are
     *  always positive values, so therefore a value of -1 is invalid.  In this
     *  structure, a value of -1 indicates that there is no buffer object, such
     *  as would be the case for a model that does not have all of the data that
     *  could be use.  For exampe, some models do not have any normals,
     *  therefore they will not have an array buffer generated for normal data.
     * */
    struct OGLMeshData {
        /// Vertex buffer ID
        GLint vertex_buffer_id = -1;
        /// UV buffer ID
        GLint uv_buffer_id = -1;
        /// Normal buffer ID
        GLint normal_buffer_id = -1;
        
        /// Vertex array object ID
        GLint vao_id = -1;

        /// Vertex count
        int vertex_count = -1;
    };

    /*!
     *  Mesh Class.
     * 
     *  The mesh class represents two separate buffers, the VBO (Vertex Buffer
     *  Object), which stores the points that make up the mesh, and the IBO
     *  (Index Buffer Object), which stores the indexes of the points that are
     *  drawn from to build the mesh.
     * 
     *  A mesh can be either 2-dimensional, in which each point in the VBO is
     *  comprised of two values (x and y), or 3-dimensional, in which each point
     *  in the VBO is comprised of three values (x, y, and z).
     * 
     *  Depending on the dimension of the mesh, the vertex buffer object will
     *  be created with either 2 times the number of indices or 3 times the
     *  number of indices.  As cool as it would be, 4 dimensional graphics are
     *  not supporeted.
     */
    class Mesh {

        private:

            /*!
             *  Calculated hash value.
             * 
             *  Meshes do not change after initialization.
             */
            uint32_t hash = 0;

            /*!
             *  Data Pointers.
             */
            MeshData data;

            /*!
             *  OpenGL data including buffer IDs and mesh properties.
             */
            OGLMeshData gldata;

            /*!
             *  Ready Flag.
             * 
             *  No attempt will be made to render this mesh until this flag is
             *  set to `true`.
             */
            bool ready = false;

            /*!
             *  Mesh Cache.
             * 
             *  Every time a mesh is loaded, it's buffer IDs will be stored here
             *  so that next time a request is made for that mesh, an ID can
             *  be located without having to re-bind the model data to the GPU.
             */
            static std::unordered_map<uint32_t, se::render::OGLMeshData> mesh_cache;

            /*!
             *  Load model data from file.
             * 
             *  This method reads and parses the contents of the given file.
             * 
             *  @param  name    Name of the file.
             */
            void load_obj(const char* fname);

            /*!
             *  File Name.
             * 
             *  For meshes that need to be loaded from disk before use.  If this
             *  value is `nullptr` when the bind method is called, the model
             *  loading process will be ignored.
             */
            const char* fname = nullptr;

        se_internal:

            /*!
             *  Bind the buffers.
             * 
             *  This method creates and transfers the data to buffers on the
             *  graphics processing device.  Once this method is called, loaded
             *  data can be safely discarded (I think)
             * 
             *  **This function should only be called from the render thread.**
             */
            void bind();

        public:

            /*!
             *  Construct a mesh.
             * 
             *  This constructor takes an array of points and saves them to the
             *  internal buffers.
             * 
             *  The mesh array will be copied into memory, so it is save to
             *  destroy the original data after calling this constructor.
             * 
             *  @param vertex_count Number of vertices in the mesh
             *  @param mesh_data    All data for the mesh
             */
            Mesh(int vertex_count, MeshData mesh_data);

            /*!
             *  Load a mesh.
             * 
             *  This contsructor takes the name of a model file and loades it
             *  into memory.
             * 
             *  @param  fname   Name of the mesh file
             */
            Mesh(const char* fname);

            /*!
             *  Destroy a mesh.
             * 
             *  Ensures all memory allocated by the mesh is released.
             */
            ~Mesh();

            /*!
             *  Get the name of the mesh.
             * 
             *  TODO: Do something with this.
             *  
             *  @return Name of the mesh
             */
            const char* name();

            /*!
             *  Check if the mesh is ready
             */
            bool is_ready();

            /*!
             *  Draw the mesh.
             * 
             *  This function binds the mesh buffers and draws the elements.
             */
            void draw();

    };

}

#endif