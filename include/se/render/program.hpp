/*!
 *  @file include/se/render.program.hpp
 * 
 *  This file defines the OpenGL shader program wrapper class template.  OpenGL
 *  programs represent a collection of shader modules/stages/objects that are
 *  used to translate geometry in 3D space, rasterize it, and shade it to a
 *  buffer or display.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_PROGRAM_H_
#define _SE_RENDER_PROGRAM_H_

#include "se/silhouette.hpp"
#include "se/render/shader.hpp"

#include <vector>

namespace se::render {

    /*!
     *  OpenGL shader program wrapper.
     * 
     *  Shader programs are collections of one or more shader
     *  modules/stages/objects that are used during runtime to define behavior
     *  for a certain set of geometry.  For example, a shader program can be
     *  used to translate a model to a certain position, rotation and scale
     *  during the vertex shading stage, then aply a texture to each polygon in
     *  the model during the fragment shading stage.
     * 
     *  This class defines an abstract interface that can be implemented by
     *  custom shader programs, such as those which require a unique combination
     *  of vertex and fragment shading capabilities.
     * 
     *  TODO: Write and insert example code here.
     */
    class Program {

        private:

            /*!
             *  Shader Program Cache.
             * 
             *  This cache is used to store the OpenGL ids of programs that have
             *  already been linked, preventing duplicates.
             */
            static std::unordered_map<uint32_t, GLuint> program_cache;

            /*!
             *  Cache lookup handler.
             * 
             *  This wrapper takes care of the program cache lookup process.
             * 
             *  @return `true` if the program was found in the cache, `false` if
             *          it was not.
             */
            bool cache_lookup();

        se_internal:

            /*!
             *  Link Callback.
             * 
             *  **Warning: This method must be called from the render thread**
             * 
             *  After a program is inserted into the linking queue, it will
             *  shortly be invoked by the render thread, by calling this method.
             *  This method performs a final cache lookup, and then invokes the
             *  virtual `link_internal()` method.
             * 
             *  This method performs a second cache lookup in case of a race
             *  condition.  The first lookup, performed when `insert()` is
             *  invoked, serves only as an optimization, and may permit multiple
             *  programs to be inserted into the linking queue if the second is
             *  inserted before the first completes compilation.
             */
            void link();

            /*!
             *  Link the Program.
             * 
             *  **Warning: This method must be called from the render thread**
             * 
             *  This process is similar to the shader module complilation
             *  process, but instead of taking source code, it takes shader
             *  modules and links them to form a program.  Additional steps are
             *  performed to configure the modules prior to linking to specify
             *  variable and buffer locations.
             * 
             *  The returned program ID will be stored in the program cache to
             *  permit skipping of future programs with the same configuration
             *  identifier.
             * 
             *  In the event of a linking error, detailed information will be
             *  logged.
             * 
             *  @return Program ID
             */
            virtual GLuint link_internal();

            /*!
             *  Set the Program ID.
             * 
             *  This method is called instead of `link()` if the render thread
             *  is able to locate the program in the cache.  It prevents
             *  multiple identical program configurations from coexisting.
             * 
             *  @param program_id   The program ID
             */
            void set_program_id(GLuint program_id);

        protected:

            /*!
             *  Module Names.
             * 
             *  This variable is only used when the default linking system is
             *  not overridden.
             * 
             *  List of shader module names to load and link for this shader
             *  program.
             */
            std::vector<const char*> default_modules;

            /*!
             *  Program ID.
             */
            GLuint program_id = 0;

            /*!
             *  Ready Flag.
             * 
             *  Until this flag is set to `true`, no attempt to enable this
             *  program will be made.
             */
            bool ready = false;

            /*!
             *  Extra Shader Definitions.
             * 
             *  If specified, this string will be prepended to the shader source
             *  during the compliation process.  If you really wanted to you
             *  could put functions or something like that in here, but that's
             *  not really what it's intended for.
             */
            const char* defines = "";

            /*!
             *  Insert this program for linking.
             * 
             *  This function serves as a wrapper for the OpenGL wrapper's  
             *  linking queue, performing a cache lookup prior to insertion.
             */
            void insert();

        public:

            /*!
             *  Construct a new instance of this program.
             */
            Program();

            /*!
             *  Destroy the program.
             */
            virtual ~Program();

            /*!
             *  Get the program name.
             * 
             *  This is currently used to identify the shader in logs, but
             *  should still attempt to remain unique.
             * 
             *  @return Program name.
             */
            virtual const char* program_name() = 0;

            /*!
             *  Get a program Hash.
             * 
             *  This method should returna number unique to this program, it's
             *  associated engine, and any variables that differentiate it from
             *  another program.  This hash is used for cache lookups, so an
             *  improperly generated hash may result in the incorrect program
             *  being associated with this object.
             * 
             *  @return Program configuration Hash.
             */
            virtual uint32_t hash() = 0;

            /*!
             *  Check if ready.
             */
            bool is_ready();

            /*!
             *  Use The Program.
             * 
             *  This method sets this program as the active OpenGL shader
             *  program that will be used for the next calls.
             * 
             *  @return `true` if the program has been enabled, or `false` if
             *          the program could not be activated.
             */
            virtual bool use();
    };
}

#endif