/*!
 *  @file include/se/render/texture.hpp
 * 
 *  Definition of the texture container.
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#ifndef _SE_RENDER_TEXTURE_H_
#define _SE_RENDER_TEXTURE_H_

#include "se/silhouette.hpp"

#include <cstdint>
#include <unordered_map>

namespace se::render {

    /*!
     *  Texture Container Class.
     * 
     *  Textures are wrappers for OpenGL 2D image buffers, known internally as
     *  textures.  This class provides functions for loading image data from
     *  disk into a texture, and in the future will support advanced operations
     *  such as rendering to a texture, and processing that texture.
     * 
     *  Currently only 24-bit RGB image data is supported.
     */
    class Texture {

        private:

            /*!
             *  Allow the render thread to access private members.
             */
            friend void render_thread_main(se::render::OpenGL* opengl);

            /*!
             *  OpenGL texture ID;
             */
            GLint texture_id = -1;

            /*!
             *  Texture width.
             */
            unsigned int width;

            /*!
             *  Texture height.
             */
            unsigned int height;

            /*!
             *  Raw image data.
             */
            uint8_t* image_data = nullptr;

            /*!
             *  Texture Hash.
             * 
             *  Unique identifier generated from the name of the texture file
             *  and the engine pointer.
             */
            uint32_t hash;

            /*!
             *  Texture cache.
             */
            static std::unordered_map<uint32_t, GLint> texture_cache;

            /*!
             *  Texture Name.
             */
            char* name;

            /*!
             *  Bind the texture.
             * 
             *  TODO: Add texture caching
             * 
             *  This method takes the image data contained in image_data and
             *  binds it to the GPU.
             */
            void bind();

            /*!
             *  Load a BMP.
             * 
             *  This function loads the requested texture file as a bitmap
             *  image.
             * 
             *  @param file     Bitmap stream.
             *  @param name     Texture name.
             */
            void load_bmp(FILE* file, const char* name);

            /*!
             *  Load a PNG.
             * 
             *  This function loads the requested texture file as a png image.
             * 
             *  @param file     PNG stream.
             *  @param name     Texture name.
             */
            void load_png(FILE* file, const char* name);

        public:

            /*!
             *  Create a new texture.
             * 
             *  This method loads an image file by the given name and binds it
             *  to the GPU.  In the event of a load failure, an explanatory
             *  message will be printed to the console.
             * 
             *  @param fname    Texture file name
             */
            Texture(const char* fname);

            /*!
             *  Use this texture.
             * 
             *  **This method should only be called from the render thread**
             * 
             *  This method activates this texture for use.
             * 
             *  If an entity, shader, or model requires multiple textures, the
             *  slot value must be incremented for each one, and the location
             *  value must match the uniform location of the texture sampler in
             *  the fragment shader.
             * 
             *  @param slot Which texture slot to bind the texture to
             *  @param location Which location to put the texture to
             */
            void use(int slot, int location);

    };

}

#endif