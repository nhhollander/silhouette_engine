##
# Blender Scene Export Addon
# 

bl_info = {
    "name": "Silhouette Engine Scene Exporter",
    "description": "Scene Exporter for Silhouette Engine",
    "category": " Import-Export",
    "author": "Nicholas Hollander",
    "blender": (2, 80, 0)
}

import bpy
import json

##
# Scene Exporter Class
#
class ExportScene(bpy.types.Operator):
    bl_idname = "se.scene_export"
    bl_label = "Export to Silhouette Engine"
   
    ##
    # Execute the exporter
    #
    def execute(self, context):
        # Get all of the objects in the scene
        objects = context.scene.objects

        # Mesh datas.
        mesh_datas = []
        mesh_names = []
        # Find mesh datas
        for obj in objects:
            # Check if this is a mesh object
            if obj.type == 'MESH':
                # Check if this is a duplicate mesh object
                if not obj.data.name in mesh_names:
                    # Found a new mesh!
                    print("Found a new mesh named [" + obj.data.name + "]")
                    mesh_names.append(obj.data.name)
                    mesh_datas.append(obj.data)
                else:
                    # Found a duplicate!
                    print("Found a duplicate of [" + obj.data.name + "]")

        # Create the export reference objects
        for mesh_data in mesh_datas:
            print("Creating new instance of [" + mesh_data.name + "]")
            # Make a new object
            obj = bpy.data.objects.new(mesh_data.name, mesh_data)
            # Put the object in the scene
            context.collection.objects.link(obj)
            # Make it the active object
            bpy.ops.object.select_all(action='DESELECT')
            context.view_layer.objects.active = obj
            obj.select_set(state=True)
            # Export the mesh as an obj
            self.export_mesh(obj, mesh_data.name)
            # Delete the mesh
            bpy.ops.object.delete()

        # Result object
        result = {
            'props': [],
            'lights': []
        }
        # Iterate over the objects
        for obj in objects:
            # Process the object appropriately
            if obj.type == 'MESH':
                result['props'].append({
                    'name': obj.name,
                    'model_name': obj.data.name,
                    'x': obj.location[0],
                    'y': obj.location[2],
                    'z': -obj.location[1],
                    'rx': obj.rotation_euler[0],
                    'ry': obj.rotation_euler[2],
                    'rz': obj.rotation_euler[1],
                    'sx': obj.scale[0],
                    'sy': obj.scale[2],
                    'sz': obj.scale[1]
                })
            if obj.type == 'LIGHT':
                result['lights'].append({
                    'name': obj.name,
                    'x': obj.location[0],
                    'y': obj.location[2],
                    'z': -obj.location[1],
                    'r': obj.data.color[0],
                    'g': obj.data.color[1],
                    'b': obj.data.color[2]
                })

        # Save the file
        sfile = open("/home/nicholas/Documents/code/Silhouette/test_scene.json","w")
        sfile.write(json.dumps(result))
        sfile.close()
        print(json.dumps(result))

        return {'FINISHED'}

    ##
    # Export a mesh object.
    #
    def export_mesh(self, obj, name):
        # Object path
        path = "/home/nicholas/Documents/code/Silhouette/data/models/"
        path += name
        path += ".obj"
        # Select the object
        obj.select_set(True)
        # Export geometry as obj
        bpy.ops.export_scene.obj(
            filepath=path,
            check_existing=False,
            axis_forward='-Z',
            axis_up='Y',
            filter_glob="*.obj",
            use_selection=True,
            use_animation=False,
            use_mesh_modifiers=True,
            use_edges=True,
            use_smooth_groups=False,
            use_smooth_groups_bitflags=False,
            use_normals=True,
            use_uvs=True,
            use_materials=False,
            use_triangles=True,
            use_nurbs=False,
            use_vertex_groups=False,
            use_blen_objects=True,
            group_by_object=False,
            group_by_material=False,
            keep_vertex_order=False,
            global_scale=1,
            path_mode='AUTO')

    ##
    # Export a light object.
    #
    def export_light(self, obj):
        # Create the result entry
        res = {
            'x': obj.location[0],
            'y': obj.location[1],
            'z': obj.location[2],
            'r': obj.data.color[0],
            'g': obj.data.color[1],
            'b': obj.data.color[2]
        }
        print(obj.data.color)
        return res

    ##
    # Export a collection
    #
    def export_instance(self, obj):
        print(dir(obj))
        print(obj.children)
        # Create the result entry
        res = {
            'name': obj.name_full,
            'x': obj.location[0],
            'y': obj.location[1],
            'z': obj.location[2],
            'rx': obj.rotation_euler[0],
            'ry': obj.rotation_euler[1],
            'rz': obj.rotation_euler[2],
            'sx': obj.scale[0],
            'sy': obj.scale[1],
            'sz': obj.scale[2]
        }
        print(json.dumps(res))
        return res

##
# Addon Registration Method
#
def register():
    print("Hello!")
    bpy.utils.register_class(ExportScene)

##
# Addon Unregistration Method
#
def unregister():
    bpy.utils.unregister_class(ExportScene)