/*!
 *  @file src/util/stringtools.cpp
 *
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "util/stringtools.hpp"

#include <cstdarg>
#include <string.h>
#include <stdio.h>

char* util::string::format(const char* fmt, ...) {

    // Get the variable arguments
    va_list args;
    va_start(args, fmt);
    // Determine the length of the string
    size_t length = vsnprintf(nullptr, 0, fmt, args);
    // Allocate the buffer
    char* buffer = new char[length + 1];
    buffer[length] = '\0';  // TODO: Is this necessary?
    // Reset the variable argument pointer
    va_start(args, fmt);
    // Generate the string
    vsprintf(&buffer[0], fmt, args);
    // Return and hope the user remembers to de-allocate
    return buffer;

}