/*!
 *  @file src/se/input/input.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/input/input.hpp"

#include <SDL2/SDL_events.h>

#include "se/input/keyHandler.hpp"
#include "se/input/mouseHandler.hpp"
#include "se/engine.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::input::Input::Input() {

}

se::input::Input::~Input() {
    
    // Set the stop flag
    this->stop_flag = true;
    // Join the input thread
    if(this->input_thread.joinable()) {
        this->input_thread.join();
    }

    DEBUG("Input handler instance destroyed!");
}

// ===================
// = PRIVATE METHODS =
// ===================

void se::input::Input::handle_key_event(SDL_KeyboardEvent* event) {
    // Get the handler list
    auto handlers = this->key_handler_map[event->keysym.sym];
    // Invoke the handlers
    for(auto handler : handlers) {
        handler->key_event(event);
    }
}

void se::input::Input::handle_mouse_event(SDL_MouseMotionEvent* event) {
    // Invoke the handlers
    for(auto handler : this->mouse_handlers) {
        handler->mouse_event(event);
    }
}

// ==================
// = PUBLIC METHODS =
// ==================

bool se::input::Input::init() {

    // Start the render thread
    DEBUG("Starting input thread");
    this->input_thread = std::thread(&se::input::Input::input_thread_main, this);

    // Initialization success
    return true;
}

void se::input::Input::stop_thread() {
    DEBUG("Stopping input thread");
    this->stop_flag = true;
    // Join the input thread
    if(this->input_thread.joinable()) {
        this->input_thread.join();
    }
    DEBUG("Input thread stopped");
}

void se::input::Input::register_key_handler(KeyHandler* handler, SDL_Keycode key) {
    // Insert
    this->key_handler_map[key].push_back(handler);
}

bool se::input::Input::deregister_key_handler(KeyHandler* handler, SDL_Keycode key) {
    // Get the vector
    auto handlers = &this->key_handler_map[key];
    // Find and remove the handler
    auto iterator = handlers->begin();
    auto end = handlers->end();
    while(iterator != end) {
        if(*iterator == handler) {
            // Remove
            handlers->erase(iterator);
            return true;
        }
    }
    // Not in map
    return false;
}

void se::input::Input::register_mouse_handler(MouseHandler* handler) {
    // Insert
    this->mouse_handlers.push_back(handler);
}

bool se::input::Input::deregister_mouse_handler(MouseHandler* handler) {
    // Find and remove the handler
    auto iterator = this->mouse_handlers.begin();
    auto end = this->mouse_handlers.end();
    while(iterator != end) {
        if(*iterator == handler) {
            // Remove
            this->mouse_handlers.erase(iterator);
            return true;
        }
    }
    // Not in map
    return false;
}


void se::input::Input::input_thread_main() {
    util::log::set_thread_name("INPUT");
    DEBUG("HELLO FROM THE INPUT THREAD!");

    // Event variable
    SDL_Event event;
    // Fun for ever and ever and ever (until you are told to stop)
    while(!this->stop_flag) {

        // Get input events
        if(SDL_PollEvent(&event)) {
            // Special quit condition (remove after debug)
            if(event.type == SDL_KEYDOWN) {
                if(event.key.keysym.sym == SDLK_ESCAPE) {
                    se::engine->quit();
                }
            }
            // Process event type
            switch(event.type) {
                case SDL_QUIT:
                    INFO("User closed program!");
                    se::engine->quit();
                    break;
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    this->handle_key_event(&event.key);
                    break;
                case SDL_MOUSEMOTION:
                    this->handle_mouse_event(&event.motion);
                    break;
                default: break;
                    //DEBUG("Unhandled event type [%s]", util::string::sdl_event_type_name(event.type));
            }
        }

        // Let the CPU cool down a bit
        std::this_thread::sleep_for(std::chrono::microseconds(10));

    }

    DEBUG("Input thread has terminated");
}