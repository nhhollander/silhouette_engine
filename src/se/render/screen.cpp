/*!
 *  @file src/se/render/screen.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/screen.hpp"

#include "se/render/camera.hpp"
#include "se/render/mesh.hpp"
#include "se/render/program/screenProgram.hpp"
#include "util/log.hpp"

#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::Screen::Screen() {
    /* Load the screen geometry.
     * Yes this is done with a model, and I know that's technically not the best
     * way to do this, but it's the easiest, and works best for this engine */
    float quad[] = {
        -1.0, -1.0, 0.0,
         1.0, -1.0, 0.0,
        -1.0,  1.0, 0.0,
        -1.0,  1.0, 0.0,
         1.0, -1.0, 0.0,
         1.0,  1.0, 0.0
    };
    se::render::MeshData md;
    md.vertex_data = quad;
    this->screen_geometry = new se::render::Mesh(6, md);
    // Create the screen program
    this->screen_program = new se::render::program::ScreenProgram();
}

se::render::Screen::~Screen() {
    // Destroy allocated resources
    delete this->screen_geometry;
    delete this->screen_program;
}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ==================
// = PUBLIC MEMBERS =
// ==================

void se::render::Screen::render(se::render::Camera* camera, se::render::Scene* scene) {
    // Render the camera view
    camera->render(scene);
    // Activate the program
    this->screen_program->use();
    // Activate the screen texture in slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, camera->get_texture_id());
    glUniform1i(SE_SHADER_LOC_TEX_SCRTEX, 0); // Attribute 1 value 0
    // Bind the computer screen instead of a virtual framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Draw the mesh
    this->screen_geometry->draw();
}