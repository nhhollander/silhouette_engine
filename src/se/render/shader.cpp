/*!
 *  @file src/se/render/shader.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/engine.hpp"
#include "se/render/shader.hpp"

#include "util/debugstrings.hpp"
#include "util/hash.hpp"
#include "util/log.hpp"

#include <GL/glew.h>
#include <string.h>
#include <string>

// Shader Cache
std::unordered_map<uint32_t, GLuint> se::render::shader_cache;

#define foobar "omnom a bongschlong"
#define barfoo 32

#define STRINGIFY_X(x) #x
#define STRINGIFY(x) STRINGIFY_X(x)

/*!
 *  Default defines for shader programs.
 */
const char* default_defines =
// Texture locations
"#define LOC_TEX_POS " STRINGIFY(SE_SHADER_LOC_TEX_POS) "\n"
"#define LOC_TEX_NORM " STRINGIFY(SE_SHADER_LOC_TEX_NORM) "\n"
"#define LOC_TEX_ALB_SPEC " STRINGIFY(SE_SHADER_LOC_TEX_ALB_SPEC) "\n"
"#define LOC_TEX_DEPTH " STRINGIFY(SE_SHADER_LOC_TEX_DEPTH) "\n"
"#define LOC_TEX_SCRTEX " STRINGIFY(SE_SHADER_LOC_TEX_SCRTEX) "\n"
"#define LOC_TEX_ALBEDO " STRINGIFY(SE_SHADER_LOC_TEX_ALBEDO) "\n"
"#define LOC_TEX_SPECULAR " STRINGIFY(SE_SHADER_LOC_TEX_SPECULAR) "\n"
// Input locations
"#define LOC_IN_VERT " STRINGIFY(SE_SHADER_LOC_IN_VERT) "\n"
"#define LOC_IN_UV " STRINGIFY(SE_SHADER_LOC_IN_UV) "\n"
"#define LOC_IN_NORM " STRINGIFY(SE_SHADER_LOC_IN_NORM) "\n"
"#define LOC_IN_MVP " STRINGIFY(SE_SHADER_LOC_IN_MVP) "\n"
"#define LOC_IN_DYN_LIGHT_COUNT " STRINGIFY(SE_SHADER_LOC_IN_DYN_LIGHT_COUNT) "\n"
"#define LOC_IN_MODEL_MAT " STRINGIFY(SE_SHADER_LOC_IN_MODEL_MAT) "\n"
"#define LOC_IN_CAM_POS " STRINGIFY(SE_SHADER_LOC_IN_CAM_POS) "\n"
"#define LOC_OUT_POS " STRINGIFY(SE_SHADER_LOC_OUT_POS) "\n"
// Output locations
"#define LOC_OUT_NORM " STRINGIFY(SE_SHADER_LOC_OUT_NORM) "\n"
"#define LOC_OUT_ALB_SPEC " STRINGIFY(SE_SHADER_LOC_OUT_ALB_SPEC) "\n"
"#define LOC_OUT_COLOR " STRINGIFY(SE_SHADER_LOC_OUT_COLOR) "\n";

/*!
 *  Compile shader.
 * 
 *  This function compiles the shader, updates the cache, and returns the shader
 *  ID unless an error is encountered that prevents compilation from finishing.
 * 
 *  An optional parameter "name" may be provided to increase the verbosity of
 *  information and error messages in the log.  If not set, it defaults to
 *  "<undefined>"
 * 
 *  @param  source  The source code of the shader
 *  @param  type    The type of the shader
 *  @param  hash    The hash to use for the cache map
 *  @param  name    [Optional] Name of shader used for logging
 *  @param  major   OpenGL major version
 *  @param  minor   OpenGL minor version
 *  @param  defines Extra defines to inject while compiling
 * 
 *  @return The shader ID, or `0` if an error is encountered
 */
GLuint compile_shader(const char* source, GLuint type, uint32_t hash,
        int major, int minor, const char* name = "undefined",
        const char* defines = ""
    ) {
    // Create shader and check for creation failure
    GLuint shader = glCreateShader(type);
    if(shader == 0) {
        ERROR("Failed to create shader [%s:%X] of type [%s] [%s(%i): %s]",
            name, hash,
            util::string::gl_shader_type(type),
            util::string::gl_error_name(glGetError()),
            glGetError(),
            util::string::gl_error_desc(glGetError()));
        return 0;
    }
    // Get the shader version
    char version[512];
    snprintf(&version[0], 512, "#version %i%i0\n", major, minor);
    // Add line directive to shader source
    char msource[strlen(source) + 512];
    snprintf(&msource[0], strlen(source) + 512, "#line 1 2\n%s", source);
    // Get the shader sources
    const char* sources[4] = {&version[0], default_defines, defines, &msource[0]};
    // Compile the shader
    glShaderSource(shader, 4, sources, nullptr);
    glCompileShader(shader);
    // Check for compilation errors
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if(compiled != GL_TRUE) {
        // Compilation failed - get and display error
        GLint error_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &error_len);
        char error_text[error_len];
        glGetShaderInfoLog(shader, error_len, &error_len, error_text);
        ERROR("Failed to compile [%s:%X]: [%s]", name, hash, error_text);
        return 0;
    }
    // Insert shader to cache
    se::render::shader_cache.insert(std::pair(hash, shader));
    return shader;
}


GLuint se::render::load_shader(const char* name, const char* defines) {
    // Sanity check for definitions
    if(defines == nullptr) {
        defines = "";
    }
    // Get the extra defines hash
    uint32_t defhash = util::hash::jenkins(strlen(defines), (uint8_t*) defines);
    uint32_t hash = util::hash::ejenkins("%s%p%x", name, engine, defhash);
    // Search the cache
    auto lookup = se::render::shader_cache.find(hash);
    if(lookup != se::render::shader_cache.end()) {
        DEBUG("Found shader [%s:%X] in cache", name, hash);
        return lookup->second;
    }
    // Determine the shader type
    GLuint shader_type = 0;
    if(strstr(name, ".vert") != nullptr || strstr(name, ".vs") != nullptr) {
        shader_type = GL_VERTEX_SHADER;
    } else if(strstr(name, ".frag") != nullptr || strstr(name, ".fs") != nullptr) {
        shader_type = GL_FRAGMENT_SHADER;
    } else {
        ERROR("Unable to load shader [%s], name does not contian valid shader type", name);
        return 0;
    }
    // Open the shader file
    std::string fname;
    fname += engine->config.get_string("app.datafolder","./");
    fname += "shaders/";
    fname += name;
    FILE* source_file = fopen(fname.c_str(), "rb");
    if(source_file == nullptr) {
        ERROR("Failed to open shader source file [%s] [%i: %s]",
            fname.c_str(), errno, strerror(errno));
        return 0;
    }
    // Find length of file
    fseek(source_file, 0, SEEK_END);
    size_t buffer_size = ftell(source_file);
    if(buffer_size < 0) {
        ERROR("Failed to locate end of source file [%s] [%i: %s]",
            fname.c_str(), errno, strerror(errno));
        return 0;
    }
    fseek(source_file, 0, SEEK_SET);
    // Read the file into memory
    char source_data[buffer_size + 1] = {0};
    if(fread(source_data, sizeof(char), buffer_size, source_file) != buffer_size) {
        if(feof(source_file)) {
            ERROR("Unexpected end of source file [%s]", fname.c_str());
        } else if(ferror(source_file)) {
            ERROR("Error reading source file [%s]", fname.c_str());
        }
        return 0;
    }
    fclose(source_file);
    // Get opengl version
    int major = engine->config.get_int("opengl.major", SE_DEFAULT_OPENGL_VERSION_MAJOR);
    int minor = engine->config.get_int("opengl.minor", SE_DEFAULT_OPENGL_VERSION_MINOR);
    // Compile the shader
    GLuint shader = compile_shader(source_data, shader_type, hash, major, minor, name, defines);
    DEBUG("Loaded shader [%s:%X] as [%u]", name, hash, shader);
    return shader;
}

GLuint se::render::load_shader(const char* source, GLuint type) {
    // Calcualte the shader hash
    uint32_t hash = util::hash::jenkins(strlen(source), (uint8_t*) source);
    // Search the cache
    auto lookup = se::render::shader_cache.find(hash);
    if(lookup != se::render::shader_cache.end()) {
        DEBUG("Found shader in cache (from raw source)");
        return lookup->second;
    }
    // Get opengl version
    int major = engine->config.get_int("opengl.major", SE_DEFAULT_OPENGL_VERSION_MAJOR);
    int minor = engine->config.get_int("opengl.minor", SE_DEFAULT_OPENGL_VERSION_MINOR);
    // Compile the shader
    GLuint shader = compile_shader(source, type, major, minor, hash);
    DEBUG("Loaded memory shader as [%u]", shader);
    return shader;
}