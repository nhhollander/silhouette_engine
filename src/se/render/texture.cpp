/*!
 *  @file src/se/render/texture.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/texture.hpp"

#include "se/engine.hpp"
#include "se/render/program.hpp"
#include "util/hash.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"

#include <string.h>
#include <GL/glew.h>
#include <png.h>

// ====================
// = STATIC VARIABLES =
// ====================

std::unordered_map<uint32_t, GLint> se::render::Texture::texture_cache;

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::Texture::Texture(const char* fname) {

    DEBUG("Loading texture [%s]", fname);

    // Save name
    this->name = new char[strlen(fname) + 1];
    strcpy(this->name, fname);

    // Calculate the texture hash
    this->hash = util::hash::jenkins(strlen(fname), (uint8_t*) fname);

    // Check the cache
    auto lookup = this->texture_cache.find(this->hash);
    if(lookup != this->texture_cache.end()) {
        DEBUG("Found texture [%s:%X] in cache (1)", fname, this->hash);
        this->texture_id = lookup->second;
        return;
    }
    // Texture not in cache :(

    // Automatically insert texture for binding
    se::engine->opengl.insert_special_function(std::bind(&se::render::Texture::bind, this));
}

// ===================
// = PRIVATE METHODS =
// ===================

void se::render::Texture::bind() {

    // Check the cache
    auto lookup = this->texture_cache.find(this->hash);
    if(lookup != this->texture_cache.end()) {
        DEBUG("Found texture [%s:%X] in cache (2)", this->name, this->hash);
        this->texture_id = lookup->second;
        return;
    }
    // Texture not in cache :(

    DEBUG("Loading texture [%s:%X]", this->name, this->hash);

    // Compound the file path
    std::string fpath;
    fpath += se::engine->config.get_string("app.datafolder","./");
    fpath += "textures/";
    fpath += this->name;

    DEBUG("File path is [%s]", fpath.c_str());

    // Open the file
    FILE* image_file = fopen(fpath.c_str(), "rb");
    if(image_file == nullptr) {
        ERROR("Failed to open texture [%s] [%i: %s]",
            fpath.c_str(), errno, strerror(errno));
        // Load the error texture
        fpath = se::engine->config.get_string("app.datafolder","./");
        fpath += "textures/error.png";
        image_file = fopen(fpath.c_str(), "rb");
        if(image_file == nullptr) {
            ERROR("Failed to load error texture! [%i: %s]",
                errno, strerror(errno));
            return;
        }
    }

    // Read enough header bytes to determine the file type
    const int header_size = 8;
    uint8_t header[header_size];
    if(fread(header, 1, header_size, image_file) != header_size) {
        ERROR("Unable to read texture header [%s] [%i: %s]",
            this->name, errno, strerror(errno));
        return;
    }
    // Check if bmp
    if(header[0] == 'B' && header[1] == 'M') {
        // It's a bitmap!
        DEBUG("Loading [%s] as BMP", this->name);
        fseek(image_file, 0, SEEK_SET);
        load_bmp(image_file, this->name);
    } else if(header[0] == 0x89 && header[1] == 0x50 && header[2] == 0x4E &&
              header[3] == 0x47 && header[4] == 0x0D && header[5] == 0x0A &&
              header[6] == 0x1A && header[7] == 0x0A) {
        // It's a PNG
        DEBUG("Loading [%s] as PNG", this->name);
        fseek(image_file, 0, SEEK_SET);
        load_png(image_file, this->name);
    } else {
        // Unknown file format
        ERROR("Unknown texture format [0x%X%X%X%X%X%X%X%X]",
            header[0], header[1], header[2], header[3],
            header[4], header[5], header[6], header[7]);
        return;
    }

    DEBUG("Binding texture [%s:%X]", this->name, this->hash);
    // Generate the texture ID
    GLuint texture_id;
    glGenTextures(1, &texture_id);
    // Bind the texture
    glBindTexture(GL_TEXTURE_2D, texture_id);
    // Pass the texture data
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGB,
        this->width,
        this->height,
        0,
        GL_BGR,
        GL_UNSIGNED_BYTE,
        this->image_data);
    // Set the texture parameters
    // TODO: Configuratlble?
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // Insert to the cache
    texture_cache.insert(std::pair(this->hash, texture_id));
    // Internally save the texture ID
    this->texture_id = texture_id;
    DEBUG("Bound texture [%s:%X] as [%i]", this->name, this->hash, texture_id);
}

void se::render::Texture::load_bmp(FILE* file, const char* name) {
    // Read the file header
    uint8_t header[54];
    if(fread(header, 1, 54, file) != 54) {
        ERROR("Unable to read texture header [%s] [%i: %s]",
            name, errno, strerror(errno));
        return;
    }
    // Get the image properties
    unsigned int data_offset;
    unsigned int image_size;
    data_offset =  *(int*)&header[0x0A];
    this->width =  *(int*)&header[0x12];
    this->height = *(int*)&header[0x16];
    image_size =   *(int*)&header[0x22];

    // Check for malformed (but fixable) data
    if(image_size == 0) {
        WARN("Texture [%s] missing image size attribute - guessing.", name);
        image_size = this->width * this->height * 3;
    }
    if(data_offset == 0) {
        WARN("Texture [%s] missing image data offset attribute - guessing.", name);
        data_offset = 54;
    }

    // Seek to data offset
    fseek(file, data_offset, SEEK_SET);
    // Clear existing image data
    if(this->image_data != nullptr) {
        delete[] this->image_data;
    }
    // Read the image data
    this->image_data = new uint8_t[image_size];
    size_t bytes_read = fread(this->image_data, 1, image_size, file);
    if(bytes_read != image_size) {
        WARN("Got [%i] bytes from [%s], expected [%i]",
            bytes_read, name, image_size);
    }
}

void se::render::Texture::load_png(FILE* file, const char* name) {
    // Initialize libPNG
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if(!png_ptr) {
        ERROR("png_create_read_struct failed!");
        return;
    }
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) {
        ERROR("png_create_info_struct failed!");
        return;
    }
    if(setjmp(png_jmpbuf(png_ptr))) {
        ERROR("Error while initializing IO");
        return;
    }
    DEBUG("Initializing PNG IO");
    png_init_io(png_ptr, file);
    png_set_sig_bytes(png_ptr, 0);
    // Read from the PNG file
    DEBUG("Reading PNG information");
    png_read_info(png_ptr, info_ptr);
    // Get file properties
    this->width = png_get_image_width(png_ptr, info_ptr);
    this->height = png_get_image_height(png_ptr, info_ptr);
    png_byte color_type = png_get_color_type(png_ptr, info_ptr);
    //png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr); // TODO: Check bit depth?  Maybe?
    // I don't know what this stuff is for
    //int number_of_passes = png_set_interlace_handling(png_ptr);
    png_read_update_info(png_ptr, info_ptr);
    // Read the file
    DEBUG("Reading PNG file");
    if(setjmp(png_jmpbuf(png_ptr))) {
        ERROR("Error during read_image");
        return;
    }
    // Create the row pointers
    png_byte** row_pointers = new png_byte*[this->height];
    // Create each row
    for(unsigned int i = 0; i < this->height; i++) {
        row_pointers[i] = new png_byte[png_get_rowbytes(png_ptr, info_ptr)];
    }
    // Read the image
    png_read_image(png_ptr, row_pointers);

    // Verify color information
    if(color_type != PNG_COLOR_TYPE_RGB) {
        ERROR("Unsupported color type [%s]",
            util::string::libpng_color_type_name(color_type));
        goto cleanup_png; // Yeah I used a goto, whatcha gonna do 'bout it?
    }

    // Allocate memory for the image
    this->image_data = new uint8_t[this->width * this->height * 3];

    // Process the image data
    DEBUG("Processing image data");
    for(unsigned int y = 0; y < this->height; y++) {
        // Get the row
        png_byte* row = row_pointers[y];
        for(unsigned int x = 0; x < this->width; x++) {
            // Get the byte
            png_byte* pixel = &(row[x * 3]);
            // Calculate image data base (png files are upside down for some reason)
            int image_data_base = (((this->height - y - 1) * this->height) + x) * 3;
            // Get the color data (and convert to BGR)
            this->image_data[image_data_base + 0] = pixel[2];
            this->image_data[image_data_base + 1] = pixel[1];
            this->image_data[image_data_base + 2] = pixel[0];
        }
    }

    // Delete memory allocated for loading the png
    cleanup_png:
    DEBUG("Cleaning up PNG data");
    // Delete each row pointer
    for(unsigned int i = 0; i < this->height; i++) {
        delete[] row_pointers[i];
    }
    // Delete the row pointers
    delete[] row_pointers;
}

// ==================
// = PUBLIC METHODS =
// ==================

void se::render::Texture::use(int unit, int location) {
    if(this->texture_id == -1) { return; }
    // Activate the texture unit
    /* According to the khronos documentation, GL_TEXTUREi will always be equal
     * to GL_TEXTURE0 + i */
    // Get the location
    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(GL_TEXTURE_2D, this->texture_id);
    glUniform1i(location, unit);
}