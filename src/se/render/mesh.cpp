/*!
 *  @file include/se/render/mesh.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/mesh.hpp"

#include <string.h>
#include <GL/glew.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "se/engine.hpp"
#include "se/render/program.hpp"
#include "util/hash.hpp"
#include "util/log.hpp"

#define DELETE_IF_NOT_NULL(S) if(S != nullptr) { \
    delete[] S; \
}

// ====================
// = STATIC VARIABLES =
// ====================

std::unordered_map<uint32_t, se::render::OGLMeshData> se::render::Mesh::mesh_cache;

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::Mesh::Mesh(int vertex_count, se::render::MeshData mesh_data) {
    DEBUG("Initializing mesh");
    // Save input variables
    this->data.vertex_count = vertex_count;

    // Calculate hash
    uint32_t vhash = 0;
    uint32_t uhash = 0;
    uint32_t nhash = 0;
    if(mesh_data.vertex_data != nullptr) {
        vhash = util::hash::jenkins(vertex_count * sizeof(float) * 3, (uint8_t*) mesh_data.vertex_data);
    }
    if(mesh_data.uv_data != nullptr) {
        uhash = util::hash::jenkins(vertex_count * sizeof(float) * 2, (uint8_t*) mesh_data.uv_data);
    }
    if(mesh_data.normal_data != nullptr) {
        nhash = util::hash::jenkins(vertex_count * sizeof(float) * 3, (uint8_t*) mesh_data.normal_data);
    }
    this->hash = util::hash::ejenkins("%X:%X:%X:%p",
        vhash, uhash, nhash, engine);

    // Check the cache
    auto lookup = this->mesh_cache.find(this->hash);
    if(lookup != this->mesh_cache.end()) {
        DEBUG("Found mesh [%s:%X] in cache(1)", this->name(), this->hash);
        this->gldata = lookup->second;
        return;
    }
    DEBUG("Mesh [%s:%X] not in cache (1)", this->name(), this->hash);

    // Process vertex data, if set
    if(mesh_data.vertex_data != nullptr) {
        this->data.vertex_data = new float[vertex_count * 3];
        memcpy(this->data.vertex_data, mesh_data.vertex_data, vertex_count * sizeof(float) * 3);
    } else {
        // Vertex data is the only mandatory data
        ERROR("Mesh [s:%X] contains no vertices!", this->name(), this->hash);
        return;
    }
    // Process UV data, if set
    if(mesh_data.uv_data != nullptr) {
        this->data.uv_data = new float[vertex_count * 2];
        memcpy(this->data.uv_data, mesh_data.uv_data, vertex_count * sizeof(float) * 2);
    }

    // Automatically insert the mesh
    se::engine->opengl.insert_special_function(std::bind(&se::render::Mesh::bind, this));
}

se::render::Mesh::Mesh(const char* fname) {

    // Get the hash
    this->hash = util::hash::jenkins(strlen(fname), (uint8_t*) fname);

    // Save the file name for binding
    this->fname = fname;

    // Check the cache
    auto lookup = this->mesh_cache.find(this->hash);
    if(lookup != this->mesh_cache.end()) {
        DEBUG("Found mesh [%s:%X] in cache", fname, this->hash);
        this->gldata = lookup->second;
        return;
    }

    // Automatically insert the mesh
    se::engine->opengl.insert_special_function(std::bind(&se::render::Mesh::bind, this));
}

se::render::Mesh::~Mesh() {
    // Free up allocated memory
    DELETE_IF_NOT_NULL(this->data.vertex_data);
    DELETE_IF_NOT_NULL(this->data.uv_data);
    DELETE_IF_NOT_NULL(this->data.normal_data);
}

// ====================
// = INTERNAL METHODS =
// ====================

void se::render::Mesh::bind() {

    // Check the cache
    auto lookup = this->mesh_cache.find(this->hash);
    if(lookup != this->mesh_cache.end()) {
        DEBUG("Found mesh [%s:%X] in cache(2)", this->name(), this->hash);
        this->gldata = lookup->second;
        // Set ready flag to begin rendering
        this->ready = true;
        return;
    }
    DEBUG("Mesh [%s:%X] not in cache (2)", this->name(), this->hash);

    // Load the object if specified
    if(this->fname != nullptr) {
        this->load_obj(this->fname);
    }

    // Create the opengl data object
    se::render::OGLMeshData gldata;

    // Generate and bind the vertex array object
    glGenVertexArrays(1, (GLuint*) &gldata.vao_id);
    glBindVertexArray(gldata.vao_id);

    // Bind vertex data if not null
    if(this->data.vertex_data != nullptr) {
        glEnableVertexAttribArray(SE_SHADER_LOC_IN_VERT);
        glGenBuffers(1, (GLuint*) &(gldata.vertex_buffer_id));
        glBindBuffer(GL_ARRAY_BUFFER, gldata.vertex_buffer_id);
        glBufferData(GL_ARRAY_BUFFER, this->data.vertex_count * sizeof(float) * 3,
            this->data.vertex_data, GL_STATIC_DRAW);
        glVertexAttribPointer(
            SE_SHADER_LOC_IN_VERT,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            NULL);
    }
    // Bind UV data if not null
    if(this->data.uv_data != nullptr) {
        glEnableVertexAttribArray(SE_SHADER_LOC_IN_UV);
        glGenBuffers(1, (GLuint*) &(gldata.uv_buffer_id));
        glBindBuffer(GL_ARRAY_BUFFER, gldata.uv_buffer_id);
        glBufferData(GL_ARRAY_BUFFER, this->data.vertex_count * sizeof(float) * 2,
            this->data.uv_data, GL_STATIC_DRAW);
        glVertexAttribPointer(
            SE_SHADER_LOC_IN_UV,
            2,
            GL_FLOAT,
            GL_FALSE,
            0,
            NULL);
    }
    // Bind Normal data if not null
    if(this->data.normal_data != nullptr) {
        glEnableVertexAttribArray(SE_SHADER_LOC_IN_NORM);
        glGenBuffers(1, (GLuint*) &(gldata.normal_buffer_id));
        glBindBuffer(GL_ARRAY_BUFFER, gldata.normal_buffer_id);
        glBufferData(GL_ARRAY_BUFFER, this->data.vertex_count * sizeof(float) * 3,
            this->data.normal_data, GL_STATIC_DRAW);
        glVertexAttribPointer(
            SE_SHADER_LOC_IN_NORM,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            NULL);
    }

    // Set the vertex count
    gldata.vertex_count = this->data.vertex_count;

    // Insert data to cache
    this->mesh_cache.insert(std::pair(hash, gldata));

    // Save the data internally.
    this->gldata = gldata;
    DEBUG("Bound mesh [%s:%X] to [%i:%i:%i]", this->name(), this->hash,
        gldata.vertex_buffer_id, gldata.uv_buffer_id, gldata.normal_buffer_id);
    // Set ready flag to begin rendering
    this->ready = true;
}

// ===================
// = PRIVATE METHODS =
// ===================

void se::render::Mesh::load_obj(const char* fname) {

    DEBUG("Loading model file [%s:%X]", fname, this->hash);

    // Open the file
    std::string fpath;
    fpath += (const char*) se::engine->config.get_string("app.datafolder","./");
    fpath += "models/";
    fpath += fname;
    // Open the file
    FILE* file = fopen(fpath.c_str(), "rb");
    if(file == nullptr) {
        ERROR("Failed to open model [%s] [%i: %s]",
            fpath.c_str(), errno, strerror(errno));
        return;
    }

    // Temporary load variables
    std::vector<unsigned int> vertex_indices;
    std::vector<unsigned int> uv_indices;
    std::vector<unsigned int> normal_indices;
    std::vector<glm::vec3> temp_vertices;
    std::vector<glm::vec2> temp_uvs;
    std::vector<glm::vec3> temp_normals;

    // Process each line in file
    while(true) {
        char line_header[128];
        // Read the first word
        int res = fscanf(file, "%s", line_header);
        if(res == EOF) { break; }

        // Process coordinate sets
        if(strcmp(line_header, "v") == 0) {
            // Vertex
            glm::vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
            temp_vertices.push_back(vertex);
        } else if(strcmp(line_header, "vt") == 0) {
            // UV
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y);
            temp_uvs.push_back(uv);
        } else if(strcmp(line_header, "vn") == 0) {
            // Normal
            glm::vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
            temp_normals.push_back(normal);
        }

        // Process faces
        else if(strcmp(line_header, "f") == 0) {
            unsigned int vertex_index[3];
            unsigned int uv_index[3];
            unsigned int normal_index[3];
            int matches = fscanf(file, "%u/%u/%u %u/%u/%u %u/%u/%u\n",
                &vertex_index[0], &uv_index[0], &normal_index[0],
                &vertex_index[1], &uv_index[1], &normal_index[1],
                &vertex_index[2], &uv_index[2], &normal_index[2]);
            if(matches != 9) {
                ERROR("Model [%s] uses an unsupported OBJ format",
                    fname);
                return;
            }
            // Save the data
            for(int i = 0; i < 3; i++) {
                vertex_indices.push_back(vertex_index[i]);
                uv_indices.push_back(uv_index[i]);
                normal_indices.push_back(normal_index[i]);
            }
        }
    }

    // Allocate space for the models
    this->data.vertex_data = new float[vertex_indices.size() * 3];
    this->data.uv_data = new float[uv_indices.size() * 2];
    this->data.normal_data = new float[normal_indices.size() * 3];

    // Process the data
    for(unsigned int i = 0; i < vertex_indices.size(); i++) {
        // Load the triangle
        unsigned int index = vertex_indices[i];
        glm::vec3 vertex = temp_vertices[index - 1];
        this->data.vertex_data[(i * 3) + 0] = vertex.x;
        this->data.vertex_data[(i * 3) + 1] = vertex.y;
        this->data.vertex_data[(i * 3) + 2] = vertex.z;
    }
    for(unsigned int i = 0; i < uv_indices.size(); i++) {
        // Load the UV coordinates
        unsigned int index = uv_indices[i];
        glm::vec2 uv = temp_uvs[index - 1];
        this->data.uv_data[(i * 2) + 0] = uv.x;
        this->data.uv_data[(i * 2) + 1] = uv.y;
    }
    for(unsigned int i = 0; i < normal_indices.size(); i++) {
        // Load the normals
        unsigned int index = normal_indices[i];
        glm::vec3 normal = temp_normals[index - 1];
        this->data.normal_data[(i * 3) + 0] = normal.x;
        this->data.normal_data[(i * 3) + 1] = normal.y;
        this->data.normal_data[(i * 3) + 2] = normal.z;
    }
    
    // Save vertex count
    this->data.vertex_count = vertex_indices.size();

}

// ==================
// = PUBLIC METHODS =
// ==================

const char* se::render::Mesh::name() {
    return "TEST_MESH";
}

bool se::render::Mesh::is_ready() {
    return this->ready;
}

void se::render::Mesh::draw() {
    // Check if ready
    if(!this->ready) { return; }
    // Bind the VAO
    glBindVertexArray(this->gldata.vao_id);
    // Draw the mesh
    glDrawArrays(GL_TRIANGLES, 0, this->gldata.vertex_count);
    // Unbind the VAO
    glBindVertexArray(0);
}