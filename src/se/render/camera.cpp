/*!
 *  @file include/se/render/camera.hpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/camera.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <cmath>
#include <GL/glew.h>

#include "se/engine.hpp"
#include "se/entity.hpp"
#include "se/render/light.hpp"
#include "se/render/scene.hpp"
#include "se/render/program/lightingProgram.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::Camera::Camera(int width, int height) {
    // Save values
    this->width = width;
    this->height = height;
    // Enable matrix inversion
    this->invert = true;
    // Load the screen model
    float quad[] = {
        -1.0, -1.0, 0.0,
         1.0, -1.0, 0.0,
        -1.0,  1.0, 0.0,
        -1.0,  1.0, 0.0,
         1.0, -1.0, 0.0,
         1.0,  1.0, 0.0
    };
    float uv[] = {
         0.0,  0.0,
         1.0,  0.0,
         0.0,  1.0,
         0.0,  1.0,
         1.0,  0.0,
         1.0,  1.0
    };
    se::render::MeshData md;
    md.vertex_data = quad;
    md.uv_data = uv;
    this->screen_mesh = new se::render::Mesh(6, md);
    // Create the lighting program
    this->lighting_program = new se::render::program::LightingProgram();
    
    // Set up the wireframe handler
    util::ConfigurationValue* wireframe = se::engine->config.get("opengl.wireframe");
    this->wireframe = wireframe->bool_;
    util::ConfigChangeHandler func =  [this](util::ConfigurationValue* value, util::Configuration* config){
        DEBUG("OpenGL wireframe mode changed!");
        this->wireframe = value->bool_;
    };
    wireframe->add_change_handler(func);

    // Get the supersample configuration option
    this->supersample =  se::engine->config.get_int("opengl.supersample", 1);

}

// ===================
// = PRIVATE METHODS =
// ===================

void se::render::Camera::configure_buffers() {

    this->config_status = 1;

    /* Configure the deferred render pass buffers */
    // Create the g buffer
    glGenFramebuffers(1, &this->g_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, this->g_buffer);
    // Create the position buffer
    glGenTextures(1, &this->position_buffer);
    glBindTexture(GL_TEXTURE_2D, this->position_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, this->width * this->supersample, this->height * this->supersample, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->position_buffer, 0);
    // Create the normal buffer
    glGenTextures(1, &this->normal_buffer);
    glBindTexture(GL_TEXTURE_2D, this->normal_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, this->width * this->supersample, this->height * this->supersample, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->normal_buffer, 0);
    // Create the albedo/specular buffer
    glGenTextures(1, &this->albedo_specular_buffer);
    glBindTexture(GL_TEXTURE_2D, this->albedo_specular_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width * this->supersample, this->height * this->supersample, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, this->albedo_specular_buffer, 0);
    // Create the depth (texture) buffer
    glGenTextures( 1, &this->depth_texture);
    glBindTexture(GL_TEXTURE_2D, this->depth_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, this->width * this->supersample, this->height * this->supersample, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->depth_texture, 0);
    // Specify the color attachment locations
    {
        unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
        glDrawBuffers(3, attachments);
    }
    // Check for configuration error
    {
        int fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if(fb_status != GL_FRAMEBUFFER_COMPLETE) {
            ERROR("Failed to configure deferred render buffers! [%X:%s]",
                fb_status, util::string::gl_framebuffer_status_name(fb_status));
            this->config_status = 3;
            return;
        }
    }

    /* Create the lighting render pass buffers */
    // Create the lighting buffer
    glGenFramebuffers(1, &this->lighting_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, this->lighting_buffer);
    // Create the lighting result buffer
    glGenTextures(1, &this->result_buffer);
    glBindTexture(GL_TEXTURE_2D, this->result_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, this->width * this->supersample, this->height * this->supersample, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->result_buffer, 0);
    // Specify the color attachment locations
    {
        unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(3, attachments);
    }
    // Check for configuration error
    {
        int fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if(fb_status != GL_FRAMEBUFFER_COMPLETE) {
            ERROR("Failed to configure lighting render buffers! [%X:%s]",
                fb_status, util::string::gl_framebuffer_status_name(fb_status));
            this->config_status = 3;
            return;
        }
    }

    this->config_status = 2;

}

// ==================
// = PUBLIC METHODS =
// ==================

void se::render::Camera::render(se::render::Scene* scene, se::render::CameraProperties* properties) {
    // Check for default
    if(scene == nullptr) {
        scene = this->default_scene;
        if(scene == nullptr) {
            WARN("Scene is null!");
            // Small delay to prevent null scene error spam
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
            return;
        }
    }
    // Check configuration status
    if(this->config_status == 0) {
        this->configure_buffers();
    }
    if(this->config_status == 3) {
        WARN("Attempted to use a broken camera");
        return;
    }
    // Properties
    CameraProperties camera_properties;
    // Get the view matrix
    glm::mat4 view_matrix = this->get_matrix();
    // Calculate the projection matrix
    glm::mat4 projection_matrix = glm::perspective(
        this->fov,
        (float) this->width / (float) this->height,
        this->clip_near,
        this->clip_far
    );
    // Calculate the camera matrix
    camera_properties.camera_matrix = projection_matrix * view_matrix;
    camera_properties.framebuffer_id = this->g_buffer;
    // Update depth if applicable
    if(properties != nullptr) {
        camera_properties.camera_depth = properties->camera_depth + 1;
    }

    /* First render pass */
    // Set resolution, if applicable
    if(this->supersample != 1) {
        glViewport(0, 0, this->width * this->supersample, this->height * this->supersample);
    }
    // Select the g-buffer
    glBindFramebuffer(GL_FRAMEBUFFER, this->g_buffer);
    // Clear the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Check for wireframe
    if(this->wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    // Render the scene
    for(auto it = scene->entities.begin(); it != scene->entities.end(); it++) {
        (*it)->draw(&camera_properties);
    }
    // Check for wireframe
    if(this->wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    /* Second render pass */
    // Select the lighting buffer
    glBindFramebuffer(GL_FRAMEBUFFER, this->lighting_buffer);
    // Clear the buffer
    glClear(GL_COLOR_BUFFER_BIT);
    // Select the lighting program
    if(!this->lighting_program->use()) {
        return;
    }
    // Send the lights
    glUniform1i(SE_SHADER_LOC_IN_DYN_LIGHT_COUNT, (int) scene->lights.size());
    int counter = 0;
    for(auto it = scene->lights.begin(); it != scene->lights.end(); it++) {
        // Send data to the shader
        glUniform4f(this->lighting_program->locations[counter].position_loc, (*it)->x, (*it)->y, (*it)->z, 1.0);
        glUniform3fv(this->lighting_program->locations[counter].color_loc, 1, &(*it)->color[0]);
        // Increment the counter
        counter++;
    }
    // Send the camera position
    glUniform3f(SE_SHADER_LOC_IN_CAM_POS, this->x, this->y, this->z);
    // Activate the textures
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->position_buffer);
    glUniform1i(SE_SHADER_LOC_TEX_POS, 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, this->normal_buffer);
    glUniform1i(SE_SHADER_LOC_TEX_NORM, 1);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, this->albedo_specular_buffer);
    glUniform1i(SE_SHADER_LOC_TEX_ALB_SPEC, 2);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, this->depth_texture);
    glUniform1i(SE_SHADER_LOC_TEX_DEPTH, 3);
    // Render the screen square
    this->screen_mesh->draw();
    // Reset the resolution, if applicable
    if(this->supersample != 1) {
        glViewport(0, 0, this->width, this->height);
    }
}

GLuint se::render::Camera::get_texture_id() {
    return this->result_buffer;
}

int se::render::Camera::ss_factor() {
    return this->supersample;
}