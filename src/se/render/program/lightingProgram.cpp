/*!
 *  @file src/render/program/lightingProgram.cpp
 *
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/program/lightingProgram.hpp"

#include "se/engine.hpp"
#include "util/hash.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"
#include "util/stringtools.hpp"

#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::program::LightingProgram::LightingProgram() :
    se::render::Program() {

    // Get the maximum number of lights
    this->max_lights = se::engine->config.get_int("opengl.max_dynamic_lights", 16);

    // Set the module names
    this->default_modules.push_back("lightingShader.vert");
    this->default_modules.push_back("lightingShader.frag");
    // Generate the defines
    this->defines = util::string::format(
        "#define MAX_DYNAMIC_LIGHTS %i\n"
        "#define SPECULAR_ENABLED %i\n",
        this->max_lights,
        se::engine->config.get_int("opengl.specular_enabled", 1)
    );
    // Insert the program for binding
    this->insert();
}

se::render::program::LightingProgram::~LightingProgram() {
    delete[] this->defines;
    delete[] this->locations;
}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ====================
// = INTERNAL MEMBERS =
// ====================

GLuint se::render::program::LightingProgram::link_internal() {
    // Call the base class
    GLuint program_id = se::render::Program::link_internal();

    // = DEBUG: DUMP ATTRIBUTE LOCATIONS AND UNIFORM INFO=
    /*{
        // Get number of attributes
        GLint count;
        glGetProgramiv(program_id, GL_ACTIVE_ATTRIBUTES, &count);
        DEBUG("There are \033[33m%i\033[0m active attributes");
        // Dump info about attributes
        GLint size;
        GLenum type;
        int buf_size = 1024;
        GLchar name[buf_size];
        GLsizei name_length;
        for(int i = 0; i < count; i++) {
            glGetActiveAttrib(program_id, (GLuint)i, buf_size, &name_length, &size, &type, name);
            DEBUG("\033[33m%i\033[0m: Name: \033[33m%s\033[0m Type: \033[33m%s\033[0m",
                i, name, util::string::gl_type_name(type));
        }
        // Get number of uniforms
        glGetProgramiv(program_id, GL_ACTIVE_UNIFORMS, &count);
        DEBUG("There are \033[33m%i\033[0m active uniforms");
        // Dump info about uniforms
        for(int i = 0; i < count; i++) {
            glGetActiveUniform(program_id, (GLuint)i, buf_size, &name_length, &size, &type, name);
            DEBUG("\033[33m%i\033[0m: Name: \033[33m%s\033[0m Type: \033[33m%s\033[0m",
                i, name, util::string::gl_type_name(type));
        }
    }*/
    // = END DEBUG =

    // Create the link location map
    this->locations = new LightLocations[this->max_lights];
    // Variable name buffer
    char vname_buf[1024];
    // Get the dynamic light locations
    for(int i = 0; i < this->max_lights; i++) {
        // Get the variable name
        sprintf(&vname_buf[0], "dynamic_lights[%i].position", i);
        this->locations[i].position_loc = glGetUniformLocation(program_id, &vname_buf[0]);
        sprintf(&vname_buf[0], "dynamic_lights[%i].color", i);
        this->locations[i].color_loc = glGetUniformLocation(program_id, &vname_buf[0]);
        //DEBUG("Located Dynamic light [#%i] at [%i:%i]", i, 
        //    this->locations[i].position_loc, this->locations[i].color_loc);
    }
    // Return the program ID
    return program_id;
}

// ==================
// = PUBLIC MEMBERS =
// ==================

uint32_t se::render::program::LightingProgram::hash() {
    // Generate a hash based on the engine
    return 0x38309d09;
}

const char* se::render::program::LightingProgram::program_name() {
    return "LIGHTING_PROGRAM";
}