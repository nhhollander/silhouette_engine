/*!
 *  @file src/render/program/staticPropShader.cpp
 *
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/program/staticPropShader.hpp"

#include "util/hash.hpp"

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::program::StaticPropShader::StaticPropShader() :
    se::render::Program() {
    // Set the module names
    this->default_modules.push_back("staticProp.vert");
    this->default_modules.push_back("staticProp.frag");
    // Automatically insert the program
    this->insert();
}

// ==================
// = PUBLIC METHODS =
// ==================

uint32_t se::render::program::StaticPropShader::hash() {
    return 0x7d411808;
}

const char* se::render::program::StaticPropShader::program_name() {
    return "STATIC_PROP_SHADER";
}