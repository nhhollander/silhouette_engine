/*!
 *  @file src/render/program/screenProgram.cpp
 *
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/program/screenProgram.hpp"

#include "se/engine.hpp"
#include "se/render/shader.hpp"
#include "util/hash.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"
#include "util/stringtools.hpp"

#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::program::ScreenProgram::ScreenProgram() :
    se::render::Program() {
    // Set the module names
    this->default_modules.push_back("screenShader.vert");
    this->default_modules.push_back("screenShader.frag");
    // Generate the defines
    this->defines = util::string::format("#define SUPERSAMPLE_FACTOR %i\n",
        se::engine->config.get_int("opengl.supersample",1));
    // Insert the program for binding
    this->insert();
}

se::render::program::ScreenProgram::~ScreenProgram() {
    delete[] this->defines;
}

// ===================
// = PRIVATE METHODS =
// ===================

// ==================
// = PUBLIC METHODS =
// ==================

uint32_t se::render::program::ScreenProgram::hash() {
    return 0x06436d9c;
}

const char* se::render::program::ScreenProgram::program_name() {
    return "SCREEN_PROGRAM";
}