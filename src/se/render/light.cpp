/*!
 *  @file src/se/render/light.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/light.hpp"

se::render::Light::Light() {}

se::render::Light::Light(float x, float y, float z, float r, float g, float b) {
    // Save the position
    this->x = x;
    this->y = y;
    this->z = z;
    // Save the color
    this->color[0] = r;
    this->color[1] = g;
    this->color[2] = b;
}