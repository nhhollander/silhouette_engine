/*!
 *  @file src/se/render/opengl.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/opengl.hpp"

#include "se/engine.hpp"
#include "se/entity.hpp"
#include "se/render/camera.hpp"
#include "se/render/mesh.hpp"
#include "se/render/program.hpp"
#include "se/render/screen.hpp"
#include "se/render/shader.hpp"
#include "se/render/texture.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"

#include <SDL2/SDL.h>
#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::OpenGL::OpenGL() {

}

se::render::OpenGL::~OpenGL() {

    // Check if the stop flag is set
    if(!this->stop_flag) {
        // Stop flag not set during de-initialization
        this->stop_flag = true;
    }

    // Join render thread if possible
    if(this->render_thread.joinable()) {
        this->render_thread.join();
    }

    // Destroy the window
    if(this->window != nullptr) {
        SDL_DestroyWindow(this->window);
    } else {
        WARN("No window to destroy");
    }

    // Clean up SDL
    SDL_Quit();

    DEBUG("OpenGL wrapper instance destroyed!");
}

// ==================
// = STATIC MEMBERS =
// ==================

void opengl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
    // Get the message information
    const char* msg_source;
    const char* msg_type;
    const char* msg_severity;
    // Get the source
    switch(source) {
        case(GL_DEBUG_SOURCE_API):
            msg_source = "API"; break;
        case(GL_DEBUG_SOURCE_WINDOW_SYSTEM):
            msg_source = "WINSYS"; break;
        case(GL_DEBUG_SOURCE_SHADER_COMPILER):
            msg_source = "SHADER_COMP"; break;
        case(GL_DEBUG_SOURCE_THIRD_PARTY):
            msg_source = "THIRD_PARTY"; break;
        case(GL_DEBUG_SOURCE_APPLICATION):
            msg_source = "APP"; break;
        case(GL_DEBUG_SOURCE_OTHER):
            msg_source = "OTHER"; break;
        default:
            msg_source = "????"; break;
    }
    // Get the type
    switch(type) {
        case(GL_DEBUG_TYPE_ERROR):
            msg_type = "ERROR"; break;
        case(GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR):
            msg_type = "DEPRECATED"; break;
        case(GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR):
            msg_type = "UNDEFINED_B"; break;
        case(GL_DEBUG_TYPE_PORTABILITY):
            msg_type = "PORTABILITY"; break;
        case(GL_DEBUG_TYPE_PERFORMANCE):
            msg_type = "PERF"; break;
        case(GL_DEBUG_TYPE_MARKER):
            msg_type = "MARKER"; break;
        case(GL_DEBUG_TYPE_PUSH_GROUP):
            msg_type = "PUSH_GROUP"; break;
        case(GL_DEBUG_TYPE_POP_GROUP):
            msg_type = "POP_GROUP"; break;
        case(GL_DEBUG_TYPE_OTHER):
            msg_type = "OTHER"; break;
        default:
            msg_type = "????"; break;
    }
    // Get the message severity
    switch(severity) {
        case(GL_DEBUG_SEVERITY_HIGH):
            msg_severity = "\033[31mHIGH\033[0m";
            break;
        case(GL_DEBUG_SEVERITY_MEDIUM):
            msg_severity = "\033[38;5;202mMEDIUM\033[0m";
            break;
        case(GL_DEBUG_SEVERITY_LOW):
            msg_severity = "\033[33mLOW\033[0m";
            break;
        case(GL_DEBUG_SEVERITY_NOTIFICATION):
            msg_severity = "\033[38;5;27mNOTIF\033[0m";
            break;
        default:
            msg_severity = "????";
            break;
    }
    // Show the message
    printf("[\033[36mOPENGL\033[0m][%s:%s:%s] %s\n" , msg_source, msg_type, msg_severity, message);
}

// ===================
// = PRIVATE METHODS =
// ===================

void se::render::OpenGL::render_thread_main() {
    util::log::set_thread_name("RENDER");
    DEBUG("HELLO FROM THE RENDER THREAD!");
    // Invoke internal initialization function
    this->init_internal();
    this->init_complete = true;
    // Timer
    auto timer = std::chrono::high_resolution_clock::now();
    // Performance variables
    uint64_t total_delay = 0;
    int frames = 0;
    // Enter the main loop
    while(!this->stop_flag) {

        // Timer start
        timer = std::chrono::high_resolution_clock::now();

        // Check for special operations
        if(!this->special_function_queue.empty()) {
            // Execute the next special function
            std::function<void()> func = this->special_function_queue.front();
            this->special_function_queue.pop();
            func();
        }

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Render the active to the screen
        if(this->active_camera != nullptr) {
            this->screen->render(this->active_camera, nullptr);
        } else {
            /* If no camera is assigned before the engine is initialized, the
             * render pass can not take place.  That's a real bummer if you ask
             * me. */
            WARN("No active camera!");
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
            continue;
        }

        // Swap display buffers
        SDL_GL_SwapWindow(this->window);

        // Calculate frame time and delay to enforce FPS cap
        auto duration = std::chrono::high_resolution_clock::now() - timer;
        int64_t delay = this->frame_time - duration.count();
        total_delay += duration.count();
        frames++;
        // Frame cap delay
        std::this_thread::sleep_for(std::chrono::nanoseconds(delay));

    }

    // Average FPS calculation
    if(frames > 0) {
        INFO("Rendered %i frames with an average duration of %fms [%f FPS]",
            frames, (total_delay / frames)/1000000.0, (frames / (total_delay / 1000000000.0)));
    }

    // End of render thread
    DEBUG("The render thread has terminated!");
}

void se::render::OpenGL::init_internal() {

    // Get opengl version
    int major = engine->config.get_int("opengl.major", SE_DEFAULT_OPENGL_VERSION_MAJOR);
    int minor = engine->config.get_int("opengl.minor", SE_DEFAULT_OPENGL_VERSION_MINOR);

    DEBUG("Using OpenGL version %i.%i", major, minor);

    // Set the SDL OpenGL attributes
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // Create the context
    this->glcontext = SDL_GL_CreateContext(this->window);
    if(this->glcontext == nullptr) {
        FATAL("Failed to create OpenGL context [%s]", SDL_GetError());
        this->init_failure = true;
        return;
    }
    DEBUG("Created OpenGL context");

    // Print device information
    DEBUG("GL_VENDOR: %s", glGetString(GL_VENDOR));
    DEBUG("GL_RENDERER: %s", glGetString(GL_RENDERER));

    // Calculate frame-time floor
    int fps_cap = se::engine->config.get_int("opengl.fps_cap", SE_DEFAULT_FPS_CAP);
    double seconds_per_frame = 1.0 / (double) fps_cap;
    this->frame_time = (unsigned int) (1000000000 * seconds_per_frame);
    DEBUG("FPS Cap is [%i fps] = [%f seconds] = [%uns]",
        fps_cap, seconds_per_frame, this->frame_time);

    // Enable "experimental" glew features (greater than OpenGL 3.0)
    glewExperimental = GL_TRUE;

    // Initialize GLEW
    GLenum glewError = glewInit();
    if(glewError != GLEW_OK) {
        FATAL("Failed to initialize GLEW [%u: %s]",
            glewError, glewGetErrorString(glewError));
        this->init_failure = true;
        return;
    }
    DEBUG("Initialized GLEW");

    // Configure v-sync mode
    if(se::engine->config.get_bool("opengl.vsync",false)) {
        if(SDL_GL_SetSwapInterval(1) < 0) {
            FATAL("Failed to enable v-sync [%s]", SDL_GetError());
            this->init_failure = true;
            return;
        }
        DEBUG("Enabled v-sync");
    } else {
        if(SDL_GL_SetSwapInterval(0) < 0) {
            FATAL("Failed to enable v-sync [%s]", SDL_GetError());
            this->init_failure = true;
            return;
        }
        DEBUG("Disabled v-sync");
    }

    // Setup the opengl debug message callback
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(opengl_debug_callback, this);

    // Set the default clear color (mustbe black for deferred rendering)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    DEBUG("Set background color");

    // Configure depth testing (required for 3d to not be janky)
	glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS); 

    // Enable Multi-Sample Anti-Aliasing (MSAA)
    glEnable(GL_MULTISAMPLE);

    return;
}

// ==================
// = PUBLIC METHODS =
// ==================

bool se::render::OpenGL::init() {

    // Initialize SDL in Video mode
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        FATAL("Failed to initialize SDL2 [%s]\n", SDL_GetError());
        return false;
    }

    // Configure MSAA
    int msaa = se::engine->config.get_int("opengl.msaa",0);
    int msaa_res = 0;
    if(msaa == 0) {
        // MSAA is disabled
        msaa_res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
        msaa_res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);
        DEBUG("Disabled MSAA");
    } else {
        // MSAA is enabled
        msaa_res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        msaa_res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, msaa);
        DEBUG("Enabled MSAA x%i", msaa);
    }
    if(msaa_res != 0) {
        WARN("Failed to configure MSAA [Enabled: %s Level: %i]",
            (msaa == 0) ? "false" : "true", msaa);
    }

    // Create the SDL window
    this->window = SDL_CreateWindow(
        se::engine->config.get_string("window.title","UNDEFINED TITLE"),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        se::engine->config.get_int("window.dimx", SE_DEFAULT_VIDEO_WIDTH),
        se::engine->config.get_int("window.dimy", SE_DEFAULT_VIDEO_HEIGHT),
        SDL_WINDOW_OPENGL);
    if(this->window == nullptr) {
        FATAL("Failed to create SDL2 Window [%s]\n", SDL_GetError());
        return false;
    }

    // Check fullscreen status
    if(se::engine->config.get_bool("window.fullscreen",false)) {
        SDL_SetWindowFullscreen(this->window, SDL_WINDOW_FULLSCREEN);
    }

    // Create the screen
    this->screen = new se::render::Screen();

    // Start the render thread
    this->render_thread = std::thread(&se::render::OpenGL::render_thread_main, this);
    // Wait for initialization to complete
    while(!this->init_complete) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    // Return result
    return !this->init_failure;
}

bool se::render::OpenGL::set_camera(se::render::Camera* camera) {
    this->active_camera = camera;
     // Assume success
    return true;
}

void se::render::OpenGL::stop_thread() {
    // Set the stop flag
    this->stop_flag = true;
    // Wait for thread termination
    if(this->render_thread.joinable()) {
        this->render_thread.join();
    }
}

void se::render::OpenGL::insert_special_function(std::function<void()> func) {
    // Insert the function to the queue
    this->special_function_queue.push(func);
}