/*!
 *  @file src/se/render/program.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/program.hpp"

#include "se/engine.hpp"
#include "se/render/shader.hpp"
#include "util/log.hpp"
#include "util/debugstrings.hpp"

#include <GL/glew.h>


std::unordered_map<uint32_t, GLuint> se::render::Program::program_cache;

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::render::Program::Program() {

}

se::render::Program::~Program() {
    // Should I be doing something here?
    // I feel like I should be doing something here.
}

// ===================
// = PRIVATE MEMBERS =
// ===================

bool se::render::Program::cache_lookup() {
    // Get the hash
    uint32_t hash = this->hash();
    // Check the cache
    auto lookup = this->program_cache.find(hash);
    if(lookup != this->program_cache.end()) {
        DEBUG("Found program [%s:%X] in cache!", this->program_name(), hash);
        // Assign the program ID
        this->set_program_id(lookup->second);
        return true;
    } else {
        DEBUG("Program [%s:%X] not in cache", this->program_name(), hash);
        return false;
    }
}

void se::render::Program::link() {
    // Final check for cache
    if(!this->cache_lookup()) {
        // Invoke the link function
        GLuint id = this->link_internal();
        this->set_program_id(id); // Just to be sure
        // Insert into the cache
        this->program_cache.insert(std::pair(this->hash(), id));
    }
}


// ====================
// = INTERNAL MEMBERS =
// ====================

GLuint se::render::Program::link_internal() {
    // Load the shader modules
    GLuint modules[this->default_modules.size()];
    for(size_t i = 0; i < this->default_modules.size(); i++) {
        modules[i] = se::render::load_shader(this->default_modules[i], this->defines);
    }
    // Create the program
    GLuint program = glCreateProgram();
    // Attach the shader modules to the program
    for(size_t i = 0; i < this->default_modules.size(); i++) {
        glAttachShader(program, modules[i]);
    }
    // Link the program
    glLinkProgram(program);
    // Check for linking failure
    GLint is_linked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &is_linked);
    if(is_linked == GL_FALSE) {
        // Get the linker error log
        GLint log_length = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
        GLchar program_log[log_length];
        glGetProgramInfoLog(program, log_length, &log_length, &program_log[0]);
        ERROR("Failed to compile shader program! [%s]", &program_log);
        // Clean up and die
        glDeleteProgram(program);
        return 0;
    }
    // Detach the shader modules
    for(size_t i = 0; i < this->default_modules.size(); i++) {
        glDetachShader(program, modules[i]);
    }
    // Linking complete!
    return program;
}

void se::render::Program::set_program_id(GLuint program_id) {
    this->program_id = program_id;
    // Set ready flag to enable this program
    this->ready = true;
}

// =====================
// = PROTECTED MEMBERS =
// =====================

void se::render::Program::insert() {
    // Check for cache
    if(!this->cache_lookup()) {
        // Insert into the link queue
        se::engine->opengl.insert_special_function(std::bind(&se::render::Program::link, this));
    }
}

// ==================
// = PUBLIC MEMBERS =
// ==================

bool se::render::Program::is_ready() {
    return this->ready;
}

bool se::render::Program::use() {
    // Check if linked
    if(this->program_id == 0) {
        WARN("Attempted to use program [%s] before it was linked!",
            this->program_name());
        return false;
    }
    // Set as the active program
    se::engine->opengl.active_program = this;
    glUseProgram(this->program_id);
    return true;
}