/*!
 *  @file include/se/render/point3d.hpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/point3d.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include "util/log.hpp"

glm::mat4 se::render::Point3D::get_translation_matrix() {
    // Get the coordinates
    float x = this->x;
    float y = this->y;
    float z = this->z;
    // Check for additive parenting
    if(this->parent_additive_mode && this->parent_translation && this->parent != nullptr) {
        x += this->parent->x;
        y += this->parent->y;
        z += this->parent->z;
    }
    // Check for inversion
    if(this->invert) {
        x = -x;
        y = -y;
        z = -z;
    }
    // Generate the matrix
    return glm::translate(glm::mat4(1), glm::vec3(x, y, z));
}

glm::mat4 se::render::Point3D::get_rotation_matrix() {
    // Get the angles
    float x = this->rx;
    float y = this->ry;
    float z = this->rz;
    // Check for adiditive parenting
    if(this->parent_additive_mode && this->parent_rotation && this->parent != nullptr) {
        x += this->parent->rx;
        y += this->parent->ry;
        z += this->parent->rz;
    }
    // Check for inversion
    if(this->invert) {
        x = -x;
        y = -y;
        z = -z;
    }
    // Generate the rotation matrices
    glm::mat4 rot_x = glm::rotate(glm::mat4(1), x, glm::vec3(1.0f, 0.0f, 0.0f));
    glm::mat4 rot_y = glm::rotate(glm::mat4(1), y, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 rot_z = glm::rotate(glm::mat4(1), z, glm::vec3(0.0f, 0.0f, 1.0f));
    // Combine and return the matrices
    if(this->invert) {
        return rot_z * rot_x * rot_y;
    } else {
        return rot_y * rot_x * rot_z;
    }
}

glm::mat4 se::render::Point3D::get_scale_matrix() {
    // Get the factors
    float x = this->sx;
    float y = this->sy;
    float z = this->sz;
    // Check for additive parenting
    if(this->parent_additive_mode && this->parent_scale && this->parent != nullptr) {
        x += this->parent->sx;
        y += this->parent->sy;
        z += this->parent->sz;
    }
    // Check for inversion
    if(this->invert) {
        x = 1/x;
        y = 1/y;
        z = 1/z;
    }
    // Generate the scale matrix
    return glm::scale(glm::mat4(1), glm::vec3(x, y, z));
}

glm::mat4 se::render::Point3D::get_matrix() {
    // Calculate the transition matrix for this point
    glm::mat4 tmatrix;
    if(this->invert) {
        tmatrix = this->get_scale_matrix() * this->get_rotation_matrix() * this->get_translation_matrix();
    } else {
        tmatrix = this->get_translation_matrix() * this->get_rotation_matrix() * this->get_scale_matrix();
    }
    // Check for parent entity
    if(this->parent != nullptr && !this->parent_additive_mode) {
        // Dirty hack for inverted parents
        bool parent_old = this->parent->invert;
        this->parent->invert = false;
        // Generate and apply the transformation
        if(this->parent_scale) {
            tmatrix = this->parent->get_scale_matrix() * tmatrix;
        }
        if(this->parent_rotation) {
            tmatrix = this->parent->get_rotation_matrix() * tmatrix;
        }
        if(this->parent_translation) {
            tmatrix = this->parent->get_translation_matrix() * tmatrix;
        }
        // Restore parental inversion
        this->parent->invert = parent_old;
    }
    return tmatrix;
}