/*!
 *  @file include/se/render/scene.hpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/render/scene.hpp"

#include <nlohmann/json.hpp>
#include <fstream>

#include "se/entity/staticProp.hpp"
#include "se/render/light.hpp"
#include "util/log.hpp"
#include "util/stringtools.hpp"

using json = nlohmann::json;

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

// ===================
// = PRIVATE METHODS =
// ===================

// ==================
// = PUBLIC METHODS =
// ==================

void se::render::Scene::load_from_file(const char* fname, se::Engine* engine) {

    DEBUG("Loading scene file!");

    // Open the scene file
    std::ifstream sfile;
    sfile.open(fname);
    // Parse the scene
    json sdata = json::parse(sfile);
    
    // Get the lights
    auto lights = sdata["lights"];
    DEBUG("There are %i lights", lights.size());
    for(auto& element : lights.items()) {
        // Get the light's properties
        auto light = element.value();
        float x = light["x"];
        float y = light["y"];
        float z = light["z"];
        float r = light["r"];
        float g = light["g"];
        float b = light["b"];
        // Create and insert a new light
        this->lights.push_back(new se::render::Light(x,y,z,r,g,b));
    }

    // Get the props
    auto props = sdata["props"];
    DEBUG("There are %i props", props.size());
    for(auto& element : props.items()) {
        // Get the prop's properties
        auto prop = element.value();
        float x = prop["x"];
        float y = prop["y"];
        float z = prop["z"];
        float rx = prop["rx"];
        float ry = prop["ry"];
        float rz = prop["rz"];
        float sx = prop["sx"];
        float sy = prop["sy"];
        float sz = prop["sz"];
        std::string model = prop["model_name"];
        DEBUG("New Prop [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] [%s]",
            x,y,z, rx,ry,rz, sx,sy,sz, model.c_str());
        // Get the model and texture names
        const char* mname = util::string::format("%s.obj", model.c_str());
        const char* tname = util::string::format("%s.png", model.c_str());
        // Create and insert a new prop
        auto nprop = new se::entity::StaticProp(mname, tname);
        nprop->x = x;
        nprop->y = y;
        nprop->z = z;
        nprop->rx = rx;
        nprop->ry = ry;
        nprop->rz = rz;
        nprop->sx = sx;
        nprop->sy = sy;
        nprop->sz = sz;
        this->entities.push_back(nprop);
        // Delete temporary variables
        delete[] mname;
        delete[] tname;
    }

    DEBUG("Loading complete!");

}