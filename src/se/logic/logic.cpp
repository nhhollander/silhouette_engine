/*!
 *  @file src/se/logic/logic.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/logic/logic.hpp"

#include "se/engine.hpp"
#include "se/logic/logicHandler.hpp"

#include "util/log.hpp"

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::logic::Logic::Logic() {

}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ==================
// = PUBLIC MEMBERS =
// ==================

bool se::logic::Logic::init() {

    // Get the timescale
    this->time_scale = se::engine->config.get_doublep("logic.timescale");

    // Calculate the loop time
    int tps = se::engine->config.get_int("logic.tps", SE_DEFAULT_TPS);
    double seconds_per_tick = 1.0 / (double) tps;
    this->tick_time = (unsigned int) (1000000000 * seconds_per_tick);
    DEBUG("TPS is [%i tps] = [%f seconds] = [%uns] = [%fms]",
        tps, seconds_per_tick, this->tick_time, this->tick_time / 1000000.0);

    // Start the logic thread
    this->logic_thread = std::thread(&se::logic::Logic::logic_thread_main, this);

    // Assume success
    return true;
}

void se::logic::Logic::stop_thread() {
    // Set the stop flag
    this->stop_flag = true;
    // Wait for thread termination
    if(this->logic_thread.joinable()) {
        this->logic_thread.join();
    }
}

void se::logic::Logic::register_object(se::logic::LogicHandler* handler) {
    this->logic_objects.push_back(handler);
}

bool se::logic::Logic::deregister_object(se::logic::LogicHandler* handler) {
    // Try to find and remove the object
    auto iterator = this->logic_objects.begin();
    auto end = this->logic_objects.end();
    while(iterator != end) {
        // Check
        if(*iterator == handler) {
            this->logic_objects.erase(iterator);
            return true;
        }
        iterator++;
    }
    // Not found
    return false;
}

void se::logic::Logic::logic_thread_main() {
    util::log::set_thread_name("LOGIC");
    DEBUG("HELLO FROM THE LOGIC THREAD!");

    // Logic timer
    auto timer = std::chrono::high_resolution_clock::now();
    auto last = timer;

    // Logic loop
    while(!this->stop_flag) {

        // Save last time and reset timer
        last = timer;
        timer = std::chrono::high_resolution_clock::now();

        // Calculate elapsed time and update clock
        uint32_t elapsed = (timer - last).count() * *(this->time_scale);
        this->time += elapsed;

        // Process logic timers
        for(auto object : this->logic_objects) {
            object->logic_event(elapsed, this->time);
        }

        // Calculate the duration and update clock
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = now - timer;
        int64_t delay = this->tick_time - duration.count();
        // Delay
        std::this_thread::sleep_for(std::chrono::nanoseconds(delay));

    }

    DEBUG("Logic thread terminated");
}