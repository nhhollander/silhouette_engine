/*!
 *  @file src/se/engine.cpp
 *
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/engine.hpp"

#include "util/log.hpp"

// ====================
// = STATIC VARIABLES =
// ====================

se::Engine* se::engine = nullptr;

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::Engine::Engine() {
    util::log::set_thread_name("MAIN");
    DEBUG("An instance of se::Engine has been created!");

    // Check if default
    if(se::engine != nullptr) {
        ERROR("Attempted to create multiple engines!  What are you doing!");
        return;
    } else {
        // Set the engine
        se::engine = this;
    }
}

se::Engine::~Engine() {
    // Destroy the renderer

    DEBUG("An instance of se::Engine has been destroyed!");
}

// ===================
// = PRIVATE METHODS =
// ===================

// ==================
// = PUBLIC METHODS =
// ==================

bool se::Engine::init() {

    DEBUG("Initializing engine...");

    // Initialize the renderer
    if(!this->opengl.init()) {
        FATAL("OpenGL initialization failed - unable to continue engine initialization.\n");
        return false;
    }

    // Initialize the input thread
    if(!this->input.init()) {
        FATAL("Input thread initialization failed");
        return false;
    }

    // Initialize logic thread
    if(!this->logic.init()) {
        FATAL("Logic thread initialization failed");
        return false;
    }
    
    INFO("Engine Initialization Complete!");
    return true;

}

void se::Engine::quit() {
    // Set the stop flag
    this->stop_flag = true;
}

void se::Engine::wait_for_quit() {
    // Wait for stop flag
    while(!this->stop_flag) {
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    }
    // Tell the renderer to stop rendering
    this->opengl.stop_thread();
    // Tell the input thread to stop listening
    this->input.stop_thread();
    // Tell the logic thread to stop logicing
    this->logic.stop_thread();
}