/*!
 *  @file src/render/program/cameraScreen.cpp
 *
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/entity/cameraScreen.hpp"

#include "se/engine.hpp"
#include "se/render/camera.hpp"
#include "se/render/mesh.hpp"
#include "util/log.hpp"
#include "se/render/program/staticPropShader.hpp"

#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::entity::CameraScreen::CameraScreen() {
    // Load the screen model
    float quad[] = {
        -1.0, -1.0, 0.0,
         1.0, -1.0, 0.0,
        -1.0,  1.0, 0.0,
        -1.0,  1.0, 0.0,
         1.0, -1.0, 0.0,
         1.0,  1.0, 0.0
    };
    float uv[] = {
         0.0,  0.0,
         1.0,  0.0,
         0.0,  1.0,
         0.0,  1.0,
         1.0,  0.0,
         1.0,  1.0
    };
    se::render::MeshData md;
    md.vertex_data = quad;
    md.uv_data = uv;
    this->mesh = new se::render::Mesh(6, md);
    // Create the program
    this->program = new se::render::program::StaticPropShader();
}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ==================
// = PUBLIC MEMBERS =
// ==================

void se::entity::CameraScreen::draw(se::render::CameraProperties* properties) {
    // Check the camera depth
    if(properties->camera_depth > 1) { return; }
    // Verify the camera
    if(this->camera == nullptr) { return; }
    // Verify the scene
    if(this->scene == nullptr) { return; }
    /* TODO: Check for recursive usage of a single camera.  Due to the nature of
     * OpenGLs frame buffers, it is not trivial to allow recursive usage of a
     * single camera.  This may change in the future, but for the time being it
     * is not possible.
     * 
     * Now interestingly enough, even though this is impossible, it does still
     * work sort of, but it is completely beyond my understanding as to why.
     */
    // Render the camera
    this->camera->render(this->scene, properties);
    // Restore the camera
    glBindFramebuffer(GL_FRAMEBUFFER, properties->framebuffer_id);
    // Select the shader program
    if(!this->program->use()) {
        return;
    }
    // Get the mvp
    glm::mat4 mvp = properties->camera_matrix * this->get_matrix();
    // Put the MVP
    glUniformMatrix4fv(3, 1, GL_FALSE, &mvp[0][0]);
    // Activate the screen texture in slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->camera->get_texture_id());
    glUniform1i(SE_SHADER_LOC_TEX_SCRTEX, 0); // Attribute 1 value 0
    // Draw the screen
    this->mesh->draw();
}