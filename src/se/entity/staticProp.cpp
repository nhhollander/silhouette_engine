/*!
 *  @file src/entity/staticProp.cpp
 *
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/entity/staticProp.hpp"

#include "se/render/camera.hpp"
#include "se/render/mesh.hpp"
#include "se/render/program/staticPropShader.hpp"
#include "se/render/texture.hpp"

#include <GL/glew.h>

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::entity::StaticProp::StaticProp(const char* model, const char* texture) {
    // Clone names
    this->model_name = strdup(model);
    this->texture_name = strdup(texture);
    // Instantiate the shader program
    this->program = new se::render::program::StaticPropShader();
    // Load the model and texture
    this->mesh = new se::render::Mesh(this->model_name);
    this->texture = new se::render::Texture(this->texture_name);
}

se::entity::StaticProp::~StaticProp() {
    delete[] this->model_name;
    delete[] this->texture_name;
}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ==================
// = PUBLIC MEMBERS =
// ==================

void se::entity::StaticProp::draw(se::render::CameraProperties* properties) {
    // TODO: Conditional rendering

    // Select the program
    if(!this->program->is_ready()) {
        return;
    }
    // Enable the program
    this->program->use();
    // Get the model matrix
    glm::mat4 model = this->get_matrix();
    // Get the MVP matrix
    glm::mat4 mvp = properties->camera_matrix * model;
    // Pass the MVP
    glUniformMatrix4fv(
        SE_SHADER_LOC_IN_MVP,
        1,
        GL_FALSE,
        &mvp[0][0]);
    glUniformMatrix4fv(
        SE_SHADER_LOC_IN_MODEL_MAT,
        1,
        GL_FALSE,
        &model[0][0]);
    // Activate the texture in slot 0
    this->texture->use(0, SE_SHADER_LOC_TEX_ALBEDO);
    // Draw the mesh
    this->mesh->draw();
}