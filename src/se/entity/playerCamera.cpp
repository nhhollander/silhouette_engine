/*!
 *  @file src/se/entity/playerCamera.cpp
 * 
 *  Copyright 2019 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "se/entity/playerCamera.hpp"

#include "util/log.hpp"

#define PI_OVER_2 1.57079632679

// ==============================
// = CONSTRUCTOR AND DESTRUCTOR =
// ==============================

se::entity::PlayerCamera::PlayerCamera(int width, int height) :
    se::render::Camera(width, height) {
    // Lock the cursor
    SDL_SetRelativeMouseMode(SDL_TRUE);
    // Register the key handlers
    se::engine->input.register_key_handler(this, SDLK_w);
    se::engine->input.register_key_handler(this, SDLK_s);
    se::engine->input.register_key_handler(this, SDLK_a);
    se::engine->input.register_key_handler(this, SDLK_d);
    // Register mouse handler
    se::engine->input.register_mouse_handler(this);
    // Register logic handler
    se::engine->logic.register_object(this);
}

// ===================
// = PRIVATE MEMBERS =
// ===================

// ==================
// = PUBLIC MEMBERS =
// ==================

void se::entity::PlayerCamera::key_event(SDL_KeyboardEvent* event) {
    bool nv = event->type == SDL_KEYDOWN;
    switch(event->keysym.sym) {
        case SDLK_w: this->key_w = nv; break;
        case SDLK_s: this->key_s = nv; break;
        case SDLK_a: this->key_a = nv; break;
        case SDLK_d: this->key_d = nv; break;
    }
}

void se::entity::PlayerCamera::mouse_event(SDL_MouseMotionEvent* event) {
    // Temporary disable movement
    return;
    // Note: Because mouse inputs are direct, they bypass the logic thread
    this->ry -= event->xrel * 0.001;
    this->rx -= event->yrel * 0.001;
    // Limit x axis
    if(this->rx < -PI_OVER_2) {
        this->rx = -PI_OVER_2;
    } else if(this->rx > PI_OVER_2) {
        this->rx = PI_OVER_2;
    }
}

void se::entity::PlayerCamera::logic_event(uint32_t elapsed, uint64_t time) {
    // Calculate distance
    float distance = (elapsed / 1000000000.0) * 3.0;

    // Rotate
    this->ry += 0.001;
    this->x = 3 * sin(this->ry);
    this->z = 3 * cos(this->ry);

    // Temporary disable movement
    return;

    // Calculate forward and backward motion
    if(this->key_w ^ this->key_s) {
        // Calculate factors
        float x_factor = sin(this->ry);
        float z_factor = cos(this->ry);
        // Apply scaled directions
        if(this->key_w) {
            this->x -= distance * x_factor;
            this->z -= distance * z_factor;
        } else {
            this->x += distance * x_factor;
            this->z += distance * z_factor;
        }
    }

    // Calculate left and right motion
    if(this->key_a ^ this->key_d) {
        // Calculate factors
        float x_factor = sin(this->ry + 1.570796326);
        float z_factor = cos(this->ry + 1.570796326);
        // Apply scaled directions
        if(this->key_a) {
            this->x -= distance * x_factor;
            this->z -= distance * z_factor;
        } else {
            this->x += distance * x_factor;
            this->z += distance * z_factor;
        }
    }
}