/*!
 *  @file src/test/main.cpp
 * 
 *  Copyright 2018 Nicholas Hollander <nhhollander@wpi.edu>
 * 
 *  Licensed under the MIT license (see LICENSE for the complete text)
 */

#include "test/main.hpp"

#include "se/entity/staticProp.hpp"
#include "util/log.hpp"
#include "util/config.hpp"
#include "se/engine.hpp"
#include "se/entity/cameraScreen.hpp"
#include "se/entity/playerCamera.hpp"
#include "se/render/scene.hpp"
#include "se/render/camera.hpp"
#include "se/render/light.hpp"


#include <stdio.h>
#include <thread>

#define FRAND(LO,HI) LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)))

int main(int argc, char* argv[]) {

    util::log::open_log_file("log.txt");

    // Create an engine instance
    se::Engine e;
    int ct = e.config.load("test.cfg");
    INFO("Loaded %i config values", ct);
    // Initialize the engine
    e.init();

    // Create a test entity
    /*test::TestEntity* te = new test::TestEntity();
    e.input.register_key_handler(te, SDLK_UP);
    e.input.register_key_handler(te, SDLK_DOWN);
    e.input.register_key_handler(te, SDLK_LEFT);
    e.input.register_key_handler(te, SDLK_RIGHT);
    e.logic.register_object(te);*/

    // Create a test scene
    se::render::Scene* test_scene = new se::render::Scene();
    test_scene->load_from_file("test_scene.json", &e);
    //test_scene->entities.push_back(te);

    // Get screen dimensions
    int width = e.config.get_int("window.dimx", SE_DEFAULT_VIDEO_WIDTH);
    int height = e.config.get_int("window.dimy", SE_DEFAULT_VIDEO_HEIGHT);

    // Create a camera
    //se::render::Camera* camera = new se::render::Camera();
    se::render::Camera* camera = new se::entity::PlayerCamera(width, height);
    camera->default_scene = test_scene;
    camera->x = 0;
    camera->y = 2;
    camera->z = 4;
    camera->rx = -.3;
    //camera->width = e.config.get<int>("window.dimx");
    //camera->height = e.config.get<int>("window.dimy");
    //camera->fov = 1.0;

    // Create a static prop
    //se::Entity* ent = new se::entity::StaticProp("suzanne.obj", "suzanne.png", &e);
    //ent->parent = camera;
    //ent->parent_additive_mode = true;
    //ent->z -= 3;
    //ent->x = 1;
    //ent->y = 1;
    //test_scene->entities.push_back(ent);

    // Create a bunch of static props
    for(int x = 0; x < 0; x++) {
        for(int y = 0; y < 10; y++) {
            se::Entity* test_ent = new se::entity::StaticProp("suzanne2.obj", "suzanne.png");
            test_ent->x = (2 * x) - 10;
            test_ent->y = .5 ;
            test_ent->z = (2 * y) - 10;
            test_ent->sx = 0.7;
            test_ent->sy = 0.7;
            test_ent->sz = 0.7;
            test_scene->entities.push_back(test_ent);
        }
    }

//
    // Create a floor
    //se::Entity* e2 = new se::entity::StaticProp("ground.obj", "uvdefault.png", &e);
    //test_scene->entities.push_back(e2);

    // Create the mystery floor
    //se::Entity* mfloor = new se::entity::StaticProp("mysteryfloor.obj", "mysteryfloor.png", &e);
    //test_scene->entities.push_back(mfloor);

    // Create a camera for the camera screen
    //se::render::Camera* cscamera = new se::render::Camera(width, height, &e);
    //cscamera->default_scene = test_scene;
    //cscamera->x = 0;
    //cscamera->z = -30;
    //cscamera->z = 5;
    //cscamera->rx = -0.4;
    //cscamera->parent = camera;
    //cscamera->parent_additive_mode = true;
    //cscamera->fov = 1.0;

    // Create a camera screen
    //se::entity::CameraScreen* cs = new se::entity::CameraScreen(&e);
    //cs->camera = cscamera;
    //cs->scene = test_scene;
    //cs->mesh = tmesh;
    //cs->y = 1.5;
    //test_scene->entities.push_back(cs);

    e.opengl.set_camera(camera);

    // Wait for termination
    e.wait_for_quit();

}