# Silhouette Engine

* [Code of Conduct](doc/conduct.md)
* [ItemDB Documentation](doc/itemdb.md)
* [Project Style Guidle](doc/styleguide.md)
* [Project Contribution Guide](doc/contribute.md)

## What is the silhouette engine?

The silhouette engine is an open-source and cross-platform game engine written
in C/C++ with support for OpenGL.

## Contributing

For information on contributing to the project, please see the [Project
Contribution Guide](doc/contribute.md)

## License

This project is licensed under the MIT License.  See [LICENSE](LICENSE) for
details.